package handlers

import (
	"encoding/json"
	"establishment-service/internal/config"
	"establishment-service/internal/convert"
	"establishment-service/internal/errproc"
	"establishment-service/internal/integrations/authservice"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"establishment-service/internal/server/models"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type EventsHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
	errProc            *errproc.ErrProc
}

type EventsDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
	ErrProc            *errproc.ErrProc
}

func NewEventsHandlers(dep EventsDependencies) (*EventsHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, ErrDepRepoIsNil
	}
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}
	if dep.ErrProc == nil {
		return nil, ErrDepErrProcIsNil
	}

	logger := dep.Logger.WithField("", "NewEventsHandlers")

	return &EventsHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
		errProc:            dep.ErrProc,
	}, nil
}

func (o *EventsHandlers) Delete(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Delete")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	vars := mux.Vars(r)
	eventGUID := vars["EventGUID"]

	err = o.repositoryRegistry.EventsRepo.Delete(repoModels.EventGUID(eventGUID))
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (o *EventsHandlers) GetAll(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetAll")
	_, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	res := o.repositoryRegistry.EventsRepo.GetByEstablishmentGUID(repoModels.EstablishmentGUID(establishmentGUID))
	result := convert.EventGetAll2Client(res)

	writeJson(w, logger, result)
}

func (o *EventsHandlers) GetAllByCity(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetAllByCity")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var decoder = schema.NewDecoder()
	var args models.EventsByCities
	err = decoder.Decode(&args, r.URL.Query())
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		return
	}

	res := o.repositoryRegistry.EventsRepo.GetAllByCity(args.City)
	result := convert.EventGetAll2Client(res)

	writeJson(w, logger, result)
}

func (o *EventsHandlers) Create(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Create")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil || resp.StatusCode != http.StatusOK {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.EventsCreateReq
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	establishment, err := o.repositoryRegistry.EstablishmentsRepo.GetOne(establishmentGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoNotFound().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, err)
		return
	}

	contains := false
	for _, v := range establishment.AdminGUIDs {
		if account.AccountGUID == v {
			contains = true
		}
	}
	if !contains {
		exception := errproc.NewException(err).WrapRepoNotFound().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, err)
		return
	}

	event, err := convert.EventCreate2Repo(&jsonRequest, establishment)
	if err != nil {
		exception := errproc.NewException(err).WrapConvertError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	err = o.repositoryRegistry.EventsRepo.Upsert(event)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.EventGetOne2Client(event)
	writeJson(w, logger, response)
}
