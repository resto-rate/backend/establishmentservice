package handlers

import (
	"encoding/json"
	"errors"
	"establishment-service/internal/config"
	"establishment-service/internal/convert"
	"establishment-service/internal/errproc"
	"establishment-service/internal/integrations/authservice"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"establishment-service/internal/server/models"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type ReviewsHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
	errProc            *errproc.ErrProc
}

type ReviewsDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
	ErrProc            *errproc.ErrProc
}

func NewReviewsHandlers(dep ReviewsDependencies) (*ReviewsHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, ErrDepRepoIsNil
	}
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}
	if dep.ErrProc == nil {
		return nil, ErrDepErrProcIsNil
	}

	logger := dep.Logger.WithField("", "NewReviewsHandlers")

	return &ReviewsHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
		errProc:            dep.ErrProc,
	}, nil
}

func (o *ReviewsHandlers) Create(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Create")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.ReviewsCreateReq
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	review, err := convert.ReviewCreateReq2Repo(&jsonRequest, establishmentGUID, account)
	if err != nil {
		exception := errproc.NewException(err).WrapConvertError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	repoReview, err := o.repositoryRegistry.ReviewsRepo.GetByReviewGUID(string(review.ReviewGUID))
	if err != nil && !errors.Is(err, repo.ErrorNotFound) {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	if repoReview != nil {
		exception := errproc.NewException(fmt.Errorf("review already exists")).WrapReviewAlreadyExists().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, fmt.Errorf("review already exists"))
		return
	}

	err = o.repositoryRegistry.ReviewsRepo.Upsert(review)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	establishment, err := o.repositoryRegistry.EstablishmentsRepo.GetOne(establishmentGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoNotFound().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var avgRating = float64(review.Rating.PriceQuality+review.Rating.Vibe+review.Rating.Service+review.Rating.Food+review.Rating.WaitingTime) / 5
	newRating := (establishment.AvgRating*float64(establishment.ReviewCount) + avgRating) / float64(establishment.ReviewCount+1)
	establishment.ReviewCount++
	establishment.AvgRating = newRating

	err = o.repositoryRegistry.EstablishmentsRepo.Upsert(establishment)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.ReviewGetOne2Client(review, establishment)
	writeJson(w, logger, response)
}

func (o *ReviewsHandlers) GetAllByReviewer(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetAllByReviewer")
	_, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	accountGUID := vars["ReviewerGUID"]

	reviews := o.repositoryRegistry.ReviewsRepo.GetByReviewerGUID(accountGUID)
	res := &models.ReviewsGetAllResponse{}
	res.Reviews = make([]models.ReviewsGetAllResponseItem, 0, len(reviews))

	for _, v := range reviews {
		establishment, err := o.repositoryRegistry.EstablishmentsRepo.GetOne(string(v.EstablishmentGUID))
		if err != nil {
			continue
		}
		res.Reviews = append(res.Reviews, models.ReviewsGetAllResponseItem{
			ReviewGUID: string(v.ReviewGUID),
			Establishment: models.ReviewsGetAllResponseItemNestedEstablishment{
				EstablishmentGUID:  string(v.EstablishmentGUID),
				EstablishmentImage: establishment.EstablishmentLogo,
				EstablishmentTitle: establishment.EstablishmentTitle,
			},
			LikedTheMost:    v.LikedTheMost,
			NeedToBeChanged: v.NeedToBeChanged,
			Comment:         v.Comment,
			Rating: models.ReviewsCreateRespNestedRating{
				Service:      v.Rating.Service,
				Food:         v.Rating.Food,
				Vibe:         v.Rating.Vibe,
				WaitingTime:  v.Rating.WaitingTime,
				PriceQuality: v.Rating.PriceQuality,
			},
			Images:    v.Images,
			Timestamp: v.Timestamp,
			User: models.ReviewsGetAllResponseItemNestedUser{
				AccountGUID: v.ReviewerGUID,
				Name:        v.User.UserName,
				Surname:     v.User.UserSurname,
				UserImage:   v.User.UserImage,
			},
		})
	}
	res.Count = len(reviews)

	writeJson(w, logger, res)
}

func (o *ReviewsHandlers) GetAllByEstablishment(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetAllByEstablishment")
	_, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	reviews := o.repositoryRegistry.ReviewsRepo.GetByEstablishmentGUID(repoModels.EstablishmentGUID(establishmentGUID))

	res := &models.ReviewsGetAllResponse{}
	res.Reviews = make([]models.ReviewsGetAllResponseItem, 0, len(reviews))
	for _, v := range reviews {
		establishment, _ := o.repositoryRegistry.EstablishmentsRepo.GetOne(string(v.EstablishmentGUID))
		res.Reviews = append(res.Reviews, models.ReviewsGetAllResponseItem{
			ReviewGUID: string(v.ReviewGUID),
			Establishment: models.ReviewsGetAllResponseItemNestedEstablishment{
				EstablishmentGUID:  string(v.EstablishmentGUID),
				EstablishmentImage: establishment.EstablishmentLogo,
				EstablishmentTitle: establishment.EstablishmentTitle,
			},
			LikedTheMost:    v.LikedTheMost,
			NeedToBeChanged: v.NeedToBeChanged,
			Comment:         v.Comment,
			Rating: models.ReviewsCreateRespNestedRating{
				Service:      v.Rating.Service,
				Food:         v.Rating.Food,
				Vibe:         v.Rating.Vibe,
				WaitingTime:  v.Rating.WaitingTime,
				PriceQuality: v.Rating.PriceQuality,
			},
			Images:    v.Images,
			Timestamp: v.Timestamp,
			User: models.ReviewsGetAllResponseItemNestedUser{
				AccountGUID: v.ReviewerGUID,
				Name:        v.User.UserName,
				Surname:     v.User.UserSurname,
				UserImage:   v.User.UserImage,
			},
		})
	}
	res.Count = len(reviews)

	writeJson(w, logger, res)
}

func (o *ReviewsHandlers) Delete(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Delete")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	reviewGUID := vars["ReviewGUID"]

	review, err := o.repositoryRegistry.ReviewsRepo.GetByReviewGUID(reviewGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoNotFound().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusBadRequest, ErrIntegrationsRedirect)
		return
	}

	if review.ReviewerGUID != account.AccountGUID {
		exception := errproc.NewException(err).WrapAccountIsNotOwner().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, ErrIntegrationsRedirect)
		return
	}

	err = o.repositoryRegistry.ReviewsRepo.Delete(repoModels.ReviewGUID(reviewGUID))
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
