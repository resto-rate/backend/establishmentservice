package handlers

import (
	"encoding/json"
	"establishment-service/internal/config"
	"establishment-service/internal/convert"
	"establishment-service/internal/errproc"
	"establishment-service/internal/integrations/authservice"
	"establishment-service/internal/repo"
	"establishment-service/internal/server/models"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type CollectionsHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
	errProc            *errproc.ErrProc
}

type CollectionsDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
	ErrProc            *errproc.ErrProc
}

func NewCollectionsHandlers(dep CollectionsDependencies) (*CollectionsHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, ErrDepRepoIsNil
	}
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}
	if dep.ErrProc == nil {
		return nil, ErrDepErrProcIsNil
	}

	logger := dep.Logger.WithField("", "NewCollectionsHandlers")

	return &CollectionsHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
		errProc:            dep.ErrProc,
	}, nil
}

func (o *CollectionsHandlers) Create(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Create")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.CollectionCreateReq
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	collection, err := convert.CollectionCreateReq2Repo(&jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapConvertError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	collection.AccountGUID = account.AccountGUID

	err = o.repositoryRegistry.CollectionsRepo.Upsert(collection)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	establishments, err := o.repositoryRegistry.EstablishmentsRepo.GetByCollectionGUID(string(collection.CollectionGUID))
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	nestedEstablishments := make([]*models.EstablishmentsGetCollectionNested, 0)
	for _, v := range establishments {
		nestedEstablishments = append(nestedEstablishments, &models.EstablishmentsGetCollectionNested{
			EstablishmentGUID:  v.EstablishmentGUID,
			EstablishmentTitle: v.EstablishmentTitle,
			Type:               string(v.Type),
			Cuisines:           v.Cuisines,
			Image:              v.EstablishmentLogo,
			AvgRating:          v.AvgRating,
			ReviewCount:        v.ReviewCount,
			AvgBill:            v.AvgBill,
		})
	}

	response := convert.CollectionOne2Client(collection, nestedEstablishments)
	writeJson(w, logger, response)
}

func (o *CollectionsHandlers) AddToCollection(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Create")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}
	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	collectionGUID := vars["CollectionGUID"]

	var jsonRequest models.AddToCollectionReq
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	jsonRequest.CollectionGUID = collectionGUID
	jsonRequest.AccountGUID = account.AccountGUID

	repoCollection, err := o.repositoryRegistry.CollectionsRepo.Get(jsonRequest.CollectionGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoNotFound().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	establishment, err := o.repositoryRegistry.EstablishmentsRepo.GetOne(jsonRequest.EstablishmentGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, err)
		return
	}

	for _, v := range establishment.CollectionGUIDs {
		if v == jsonRequest.CollectionGUID {
			exception := errproc.NewException(fmt.Errorf("establishment already in collection")).WrapEstablishmentAlreadyInCollection().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusForbidden, fmt.Errorf("establishment already in collection"))
			return
		}
	}

	repoCollection.EstablishmentCount++
	if repoCollection.EstablishmentCount == 1 {
		repoCollection.Preview = establishment.EstablishmentLogo
	}

	err = o.repositoryRegistry.CollectionsRepo.Upsert(repoCollection)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	establishment.CollectionGUIDs = append(establishment.CollectionGUIDs, collectionGUID)
	err = o.repositoryRegistry.EstablishmentsRepo.Upsert(establishment)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	establishments, err := o.repositoryRegistry.EstablishmentsRepo.GetByCollectionGUID(collectionGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	nestedEstablishments := make([]*models.EstablishmentsGetCollectionNested, 0)
	for _, v := range establishments {
		nestedEstablishments = append(nestedEstablishments, &models.EstablishmentsGetCollectionNested{
			EstablishmentGUID:  v.EstablishmentGUID,
			EstablishmentTitle: v.EstablishmentTitle,
			Type:               string(v.Type),
			Cuisines:           v.Cuisines,
			Image:              v.EstablishmentLogo,
			AvgRating:          v.AvgRating,
			ReviewCount:        v.ReviewCount,
			AvgBill:            v.AvgBill,
		})
	}

	response := convert.CollectionOne2Client(repoCollection, nestedEstablishments)
	writeJson(w, logger, response)
}

func (o *CollectionsHandlers) DeleteFromCollection(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "DeleteFromCollection")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}
	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	collectionGUID := vars["CollectionGUID"]
	establishmentGUID := vars["EstablishmentGUID"]

	establishment, err := o.repositoryRegistry.EstablishmentsRepo.GetOne(establishmentGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	for i, v := range establishment.CollectionGUIDs {
		if v == collectionGUID {
			establishment.CollectionGUIDs = append(establishment.CollectionGUIDs[:i], establishment.CollectionGUIDs[i+1:]...)
			break
		}
	}

	err = o.repositoryRegistry.EstablishmentsRepo.Upsert(establishment)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (o *CollectionsHandlers) GetOne(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetOne")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	collectionGUID := vars["CollectionGUID"]

	collection, err := o.repositoryRegistry.CollectionsRepo.Get(collectionGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	establishments, err := o.repositoryRegistry.EstablishmentsRepo.GetByCollectionGUID(string(collection.CollectionGUID))
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	nestedEstablishments := make([]*models.EstablishmentsGetCollectionNested, 0)
	for _, v := range establishments {
		nestedEstablishments = append(nestedEstablishments, &models.EstablishmentsGetCollectionNested{
			EstablishmentGUID:  v.EstablishmentGUID,
			EstablishmentTitle: v.EstablishmentTitle,
			Type:               string(v.Type),
			Cuisines:           v.Cuisines,
			Image:              v.EstablishmentLogo,
			AvgRating:          v.AvgRating,
			ReviewCount:        v.ReviewCount,
			AvgBill:            v.AvgBill,
		})
	}

	response := convert.CollectionOne2Client(collection, nestedEstablishments)

	writeJson(w, logger, response)
}

func (o *CollectionsHandlers) GetByAccount(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetByAccount")
	_, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	accountGUID := vars["AccountGUID"]

	collections := o.repositoryRegistry.CollectionsRepo.GetByAccountGUID(accountGUID)
	response := convert.CollectionsRepo2Client(collections)

	writeJson(w, logger, response)
}
