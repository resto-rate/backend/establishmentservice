package handlers

import (
	"encoding/json"
	"establishment-service/internal/config"
	"establishment-service/internal/convert"
	"establishment-service/internal/errproc"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"establishment-service/internal/server/models"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type DishesHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
	errProc            *errproc.ErrProc
}

type DishesDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
	ErrProc            *errproc.ErrProc
}

func NewDishesHandlers(dep DishesDependencies) (*DishesHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, ErrDepRepoIsNil
	}
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}
	if dep.ErrProc == nil {
		return nil, ErrDepErrProcIsNil
	}

	logger := dep.Logger.WithField("", "NewDishesHandlers")

	return &DishesHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
		errProc:            dep.ErrProc,
	}, nil
}

func (o *DishesHandlers) GetAll(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetAll")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	menu, err := o.repositoryRegistry.DishesRepo.GetAll(repoModels.EstablishmentGUID(establishmentGUID))
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.DishesGetAll2Client(menu)
	writeJson(w, logger, response)
}

func (o *DishesHandlers) Delete(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Create")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	dishGUID := vars["DishGUID"]

	err = o.repositoryRegistry.DishesRepo.Delete(dishGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (o *DishesHandlers) Create(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Create")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.CreateDishReq
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	dish, err := convert.DishCreate2Repo(&jsonRequest, establishmentGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapConvertError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	err = o.repositoryRegistry.DishesRepo.Upsert(dish)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.DishGetOne2Client(dish)
	writeJson(w, logger, response)
}
