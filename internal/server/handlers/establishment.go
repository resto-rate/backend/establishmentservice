package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"establishment-service/internal/config"
	"establishment-service/internal/convert"
	"establishment-service/internal/errproc"
	"establishment-service/internal/integrations"
	"establishment-service/internal/integrations/authservice"
	"establishment-service/internal/integrations/notificationservice"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"establishment-service/internal/server/models"
	"fmt"
	"github.com/ekomobile/dadata/v2/api/model"
	"github.com/ekomobile/dadata/v2/api/suggest"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type EstablishmentHandlers struct {
	repositoryRegistry *repo.Registry
	logger             logrus.FieldLogger
	conf               *config.Config
	errProc            *errproc.ErrProc
	daDataApi          *suggest.Api
}

type EstablishmentDependencies struct {
	RepositoryRegistry *repo.Registry
	Logger             logrus.FieldLogger
	Conf               *config.Config
	ErrProc            *errproc.ErrProc
	DaDataApi          *suggest.Api
}

func NewEstablishmentHandlers(dep EstablishmentDependencies) (*EstablishmentHandlers, error) {

	if dep.RepositoryRegistry == nil {
		return nil, ErrDepRepoIsNil
	}
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}
	if dep.ErrProc == nil {
		return nil, ErrDepErrProcIsNil
	}
	if dep.DaDataApi == nil {
		return nil, ErrDepErrDaDataIsNil
	}

	logger := dep.Logger.WithField("", "EstablishmentHandlers")

	return &EstablishmentHandlers{
		repositoryRegistry: dep.RepositoryRegistry,
		logger:             logger,
		conf:               dep.Conf,
		errProc:            dep.ErrProc,
		daDataApi:          dep.DaDataApi,
	}, nil
}

func (o *EstablishmentHandlers) GetInputVars(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetTypes")
	_, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	types := o.repositoryRegistry.EstablishmentsRepo.GetTypes()
	cuisinesTypes := o.repositoryRegistry.EstablishmentsRepo.GetCuisinesTypes()
	suitableCases := o.repositoryRegistry.EstablishmentsRepo.GetSuitableCases()
	additionalServices := o.repositoryRegistry.EstablishmentsRepo.GetAdditionalServices()
	dishCategories := o.repositoryRegistry.DishesRepo.GetCategories()

	res := convert.InputTypesGetAll2Client(convert.InputTypesGetAllArgs{EstTypes: types, DishCategories: dishCategories,
		CuisineTypes: cuisinesTypes, SuitableCases: suitableCases, AdditionalServices: additionalServices})

	writeJson(w, logger, res)
}

func (o *EstablishmentHandlers) GetOne(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetOne")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	establishment, err := o.repositoryRegistry.EstablishmentsRepo.GetOne(establishmentGUID)
	if err != nil {
		if errors.Is(err, repo.ErrorNotFound) {
			exception := errproc.NewException(err).WrapRepoNotFound().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusNotFound, err)
			return
		}
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	favDishes, err := o.repositoryRegistry.DishesRepo.GetFavouritesByEstablishment(repoModels.EstablishmentGUID(establishmentGUID))
	if err != nil {
		if errors.Is(err, repo.ErrorNotFound) {
			exception := errproc.NewException(err).WrapRepoNotFound().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusNotFound, err)
			return
		}
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	reviews := o.repositoryRegistry.ReviewsRepo.GetByEstablishmentGUID(repoModels.EstablishmentGUID(establishmentGUID))

	response := convert.EstablishmentGetOne2Client(establishment, favDishes, reviews)
	writeJson(w, logger, response)
}

func (o *EstablishmentHandlers) Search(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Search")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var tempCity string
	accessToken := r.Header.Get("Authorization")
	if accessToken != "" {
		resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
		if err != nil {
			exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
			return
		}

		var account authservice.Account
		responseIntegrationBody, err := io.ReadAll(resp.Body)
		if err != nil {
			exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}
		err = json.Unmarshal(responseIntegrationBody, &account)
		if err != nil {
			exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}

		tempCity = account.City
	}

	var decoder = schema.NewDecoder()
	var args models.EstablishmentSearchArgs
	err = decoder.Decode(&args, r.URL.Query())
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		return
	}

	args.ItemPerPage = o.conf.FullTextItemCount
	if args.City == nil {
		args.City = &tempCity
	}
	repoArgs := convert.EstablishmentSearchClientReq2Repo(&args)

	estGUIDsByDishes := o.repositoryRegistry.DishesRepo.GetEstablishmentsForSearch(args.FullTextFind)
	repoArgs.EstablishmentGUIDs = estGUIDsByDishes

	establishments, err := o.repositoryRegistry.EstablishmentsRepo.Search(repoArgs)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.EstablishmentsSearchRepo2Client(&establishments)

	writeJson(w, logger, response)
}

func (o *EstablishmentHandlers) Create(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Create")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapRequestBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var jsonRequest models.CreateEstablishmentReq
	jsonRequest.AdminGUIDs = append(jsonRequest.AdminGUIDs, account.AccountGUID)
	err = json.Unmarshal(requestBody, &jsonRequest)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	params := suggest.RequestParams{
		Query:   jsonRequest.Address,
		ToBound: &suggest.Bound{Value: model.BoundValue("House")},
	}
	addresses, err := o.daDataApi.Address(context.Background(), &params)
	if err != nil {
		exception := errproc.NewException(err).WrapDaDataGetAddressError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	establishment, err := convert.EstablishmentCreate2Repo(&jsonRequest, addresses)
	if err != nil {
		exception := errproc.NewException(err).WrapConvertError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	err = o.repositoryRegistry.EstablishmentsRepo.Upsert(establishment)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	err = authservice.SetRestAdmin(o.conf.AuthServiceAddress, account.AccountGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accountsResponse, err := authservice.GetByAddress(o.conf.AuthServiceAddress, establishment.Address)
	if err != nil {
		exception := errproc.NewException(err).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
	} else {
		var usersByAddress authservice.GetUsersByAddress
		responseUsersByAddress, err := io.ReadAll(accountsResponse.Body)
		if err != nil {
			exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}
		err = json.Unmarshal(responseUsersByAddress, &usersByAddress)
		if err != nil {
			exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}

		_, err = notificationservice.SendNotification(o.conf.NotificationServiceAddress, notificationservice.Notification{
			Message: notificationservice.NotificationNestedMessage{
				Title: fmt.Sprintf("%s в вашем городе!", establishment.EstablishmentTitle),
				Body:  fmt.Sprintf("В приложении новое заведение в вашем городе!"),
				Image: establishment.EstablishmentLogo,
			},
			Users: usersByAddress.Users,
		})
		if err != nil {
			exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, err)
			return
		}
	}

	response := convert.EstablishmentGetOne2Client(establishment, nil, nil)

	writeJson(w, logger, response)
}

func (o *EstablishmentHandlers) GetInputAddress(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetInputAddress")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	var decoder = schema.NewDecoder()
	var args models.EstablishmentGetInputAddressArgs
	err = decoder.Decode(&args, r.URL.Query())
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		return
	}

	params := suggest.RequestParams{
		Query:   args.FullTextFind,
		ToBound: &suggest.Bound{Value: model.BoundValue("House")},
	}
	addresses, err := o.daDataApi.Address(context.Background(), &params)
	if err != nil {
		exception := errproc.NewException(err).WrapDaDataGetAddressError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.EstablishmentGetInputAddress2Client(addresses)

	writeJson(w, logger, response)
}

func (o *EstablishmentHandlers) GetByAdminGUID(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "GetByAdminGUID")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	AdminGUID := vars["AdminGUID"]

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		if errors.Is(err, integrations.ErrNewRequestToIAuthService) {
			exception := errproc.NewException(integrations.ErrNewRequestToIAuthService).WrapIntegrationError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrNewRequestToIAuthService)
			return
		}
		if errors.Is(err, integrations.ErrDoRequestToAuthService) {
			exception := errproc.NewException(integrations.ErrDoRequestToAuthService).WrapIntegrationError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrDoRequestToAuthService)
			return
		}
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	if AdminGUID != account.AccountGUID {
		exception := errproc.NewException(err).WrapAccountGUIDsCompare().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusForbidden, err)
		return
	}

	establishments, err := o.repositoryRegistry.EstablishmentsRepo.GetByAdminGUID(AdminGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	response := convert.EstablishmentsSearchRepo2Client(&establishments)

	writeJson(w, logger, response)
}

func (o *EstablishmentHandlers) Delete(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "Delete")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := authservice.GetProfile(o.conf.AuthServiceAddress, r)
	if err != nil {
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	var account authservice.Account
	responseIntegrationBody, err := io.ReadAll(resp.Body)
	if err != nil {
		exception := errproc.NewException(err).WrapResponseIntegrationBodyReadError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	err = json.Unmarshal(responseIntegrationBody, &account)
	if err != nil {
		exception := errproc.NewException(err).WrapJSONUnmarshalError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	vars := mux.Vars(r)
	establishmentGUID := vars["EstablishmentGUID"]

	err = o.repositoryRegistry.EstablishmentsRepo.Delete(establishmentGUID)
	if err != nil {
		exception := errproc.NewException(err).WrapRepoError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}
