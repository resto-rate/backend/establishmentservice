package handlers

import (
	"establishment-service/internal/config"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"net/http"
)

type MiddlewareI interface {
	Middleware(next http.Handler) http.Handler
}

type Middlewares struct {
	RequestIDMiddleware MiddlewareI
}

type Establishment interface {
	GetOne(w http.ResponseWriter, r *http.Request)
	Search(w http.ResponseWriter, r *http.Request)
	Create(w http.ResponseWriter, r *http.Request)
	GetInputVars(w http.ResponseWriter, r *http.Request)
	GetInputAddress(w http.ResponseWriter, r *http.Request)
	GetByAdminGUID(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}

type Dishes interface {
	GetAll(w http.ResponseWriter, r *http.Request)
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}

type Promotions interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetAll(w http.ResponseWriter, r *http.Request)
}

type Events interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetAll(w http.ResponseWriter, r *http.Request)
	GetAllByCity(w http.ResponseWriter, r *http.Request)
}

type Reviews interface {
	Create(w http.ResponseWriter, r *http.Request)
	GetAllByReviewer(w http.ResponseWriter, r *http.Request)
	GetAllByEstablishment(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}

type Collections interface {
	Create(w http.ResponseWriter, r *http.Request)
	AddToCollection(w http.ResponseWriter, r *http.Request)
	GetOne(w http.ResponseWriter, r *http.Request)
	GetByAccount(w http.ResponseWriter, r *http.Request)
	DeleteFromCollection(w http.ResponseWriter, r *http.Request)
}

type Service interface {
	Health(w http.ResponseWriter, r *http.Request)
	UploadImage(w http.ResponseWriter, r *http.Request)
}

type Handlers struct {
	Establishment Establishment
	Dishes        Dishes
	Promotions    Promotions
	Events        Events
	Reviews       Reviews
	Collections   Collections
	Service       Service
}

type RouteDependencies struct {
	Logger      logrus.FieldLogger
	Middlewares Middlewares
	Handlers    Handlers

	Conf *config.Config
}

func NewRoute(dep RouteDependencies) http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/health", dep.Handlers.Service.Health)

	establishments := r.PathPrefix("/establishments").Subrouter()
	establishments.Use(dep.Middlewares.RequestIDMiddleware.Middleware)

	// swagger:route GET /establishments/{EstablishmentGUID} getOneEstablishment getOneEstablishment
	//
	// Return all available establishments methods
	//
	//     Parameters: []
	//
	//     Responses:
	//       200:
	//		 	description: status ok
	//			schema:
	//				$ref: "#/model/AuthMethodsResponse"
	//       500:
	establishments.HandleFunc("/search", dep.Handlers.Establishment.Search).Methods(http.MethodGet)
	establishments.HandleFunc("/create", dep.Handlers.Establishment.Create).Methods(http.MethodPost)
	establishments.HandleFunc("/inputvars", dep.Handlers.Establishment.GetInputVars).Methods(http.MethodGet)
	establishments.HandleFunc("/inputaddress", dep.Handlers.Establishment.GetInputAddress).Methods(http.MethodGet)
	establishments.HandleFunc("/{EstablishmentGUID}", dep.Handlers.Establishment.GetOne).Methods(http.MethodGet)
	establishments.HandleFunc("/{AdminGUID}/getall", dep.Handlers.Establishment.GetByAdminGUID).Methods(http.MethodGet)
	establishments.HandleFunc("/{EstablishmentGUID}", dep.Handlers.Establishment.Delete).Methods(http.MethodDelete)

	establishments.HandleFunc("/{EstablishmentGUID}/menu/create", dep.Handlers.Dishes.Create).Methods(http.MethodPost)
	establishments.HandleFunc("/{EstablishmentGUID}/menu", dep.Handlers.Dishes.GetAll).Methods(http.MethodGet)
	establishments.HandleFunc("/{EstablishmentGUID}/menu/{DishGUID}", dep.Handlers.Dishes.Delete).Methods(http.MethodDelete)

	establishments.HandleFunc("/{EstablishmentGUID}/promotions/create", dep.Handlers.Promotions.Create).Methods(http.MethodPost)
	establishments.HandleFunc("/{EstablishmentGUID}/promotions/{PromotionGUID}", dep.Handlers.Promotions.Delete).Methods(http.MethodDelete)
	establishments.HandleFunc("/{EstablishmentGUID}/promotions", dep.Handlers.Promotions.GetAll).Methods(http.MethodGet)

	establishments.HandleFunc("/{EstablishmentGUID}/events/create", dep.Handlers.Events.Create).Methods(http.MethodPost)
	establishments.HandleFunc("/{EstablishmentGUID}/events/{EventGUID}", dep.Handlers.Events.Delete).Methods(http.MethodDelete)
	establishments.HandleFunc("/{EstablishmentGUID}/events", dep.Handlers.Events.GetAll).Methods(http.MethodGet)
	establishments.HandleFunc("/events/bycity", dep.Handlers.Events.GetAllByCity).Methods(http.MethodGet)

	establishments.HandleFunc("/{EstablishmentGUID}/review/create", dep.Handlers.Reviews.Create).Methods(http.MethodPost)
	establishments.HandleFunc("/reviewers/{ReviewerGUID}/reviews", dep.Handlers.Reviews.GetAllByReviewer).Methods(http.MethodGet)
	establishments.HandleFunc("/{EstablishmentGUID}/reviews", dep.Handlers.Reviews.GetAllByEstablishment).Methods(http.MethodGet)
	establishments.HandleFunc("/reviews/{ReviewGUID}", dep.Handlers.Reviews.Delete).Methods(http.MethodDelete)

	establishments.HandleFunc("/collections/create", dep.Handlers.Collections.Create).Methods(http.MethodPost)
	establishments.HandleFunc("/collections/{CollectionGUID}/add", dep.Handlers.Collections.AddToCollection).Methods(http.MethodPost)
	establishments.HandleFunc("/collections/{CollectionGUID}", dep.Handlers.Collections.GetOne).Methods(http.MethodGet)
	establishments.HandleFunc("/users/{AccountGUID}/collections", dep.Handlers.Collections.GetByAccount).Methods(http.MethodGet)
	establishments.HandleFunc("/collections/{CollectionGUID}/delete/{EstablishmentGUID}", dep.Handlers.Collections.DeleteFromCollection).Methods(http.MethodDelete)

	establishments.HandleFunc("/uploadImage", dep.Handlers.Service.UploadImage).Methods(http.MethodPost)

	corsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Origin", "Authorization", "Content-Type"},
	})

	return corsHandler.Handler(r)
}
