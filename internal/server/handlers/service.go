package handlers

import (
	"errors"
	"establishment-service/internal/config"
	"establishment-service/internal/convert"
	"establishment-service/internal/errproc"
	"establishment-service/internal/integrations"
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
)

type ServiceHandlers struct {
	logger  logrus.FieldLogger
	conf    *config.Config
	errProc *errproc.ErrProc
}

type ServiceDependencies struct {
	Logger  logrus.FieldLogger
	Conf    *config.Config
	ErrProc *errproc.ErrProc
}

func NewServiceHandlers(dep ServiceDependencies) (*ServiceHandlers, error) {
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}

	logger := dep.Logger.WithField("EstablishmentService", "ServiceHandlersCI")

	authHandlers := &ServiceHandlers{
		logger: logger,
		conf:   dep.Conf,
	}

	return authHandlers, nil
}

func (o *ServiceHandlers) Health(w http.ResponseWriter, r *http.Request) {
	writeJson(w, nil, "Hello")
}

func (o *ServiceHandlers) UploadImage(w http.ResponseWriter, r *http.Request) {
	logger, handlerData := o.errProc.NewHandlerLogger(r, o.logger, "UploadImage")
	errproc, err := o.errProc.NewHandlerErrProc(r, logger, handlerData)
	if err != nil {
		logger.Error(err)
		writeError(w, logger, http.StatusInternalServerError, err)
		return
	}
	accessToken := r.Header.Get("Authorization")
	if accessToken == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	resp, err := integrations.RedirectUploadImageFile(o.conf.ImageServiceAddress, r)
	if err != nil {
		if errors.Is(err, integrations.ErrGetFormFile) {
			exception := errproc.NewException(integrations.ErrGetFormFile).WrapIntegrationNotFoundRouteError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrGetFormFile)
			return
		}
		if errors.Is(err, integrations.ErrCreateFormFile) {
			exception := errproc.NewException(integrations.ErrCreateFormFile).WrapIntegrationNotFoundRouteError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrCreateFormFile)
			return
		}
		if errors.Is(err, integrations.ErrCopyFormFile) {
			exception := errproc.NewException(integrations.ErrCopyFormFile).WrapIntegrationNotFoundRouteError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrCopyFormFile)
			return
		}
		if errors.Is(err, integrations.ErrCloseWriter) {
			exception := errproc.NewException(integrations.ErrCloseWriter).WrapIntegrationNotFoundRouteError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrCloseWriter)
			return
		}
		if errors.Is(err, integrations.ErrNewRequestToImageService) {
			exception := errproc.NewException(integrations.ErrNewRequestToImageService).WrapIntegrationNotFoundRouteError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrNewRequestToImageService)
			return
		}
		if errors.Is(err, integrations.ErrDoRequestToImageService) {
			exception := errproc.NewException(integrations.ErrDoRequestToImageService).WrapIntegrationNotFoundRouteError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrDoRequestToImageService)
			return
		}
		if errors.Is(err, integrations.ErrNotFoundImageService) {
			exception := errproc.NewException(integrations.ErrNotFoundImageService).WrapIntegrationNotFoundRouteError().CaptureSentry()
			exception.ConsolePrint()
			writeError(w, logger, http.StatusInternalServerError, integrations.ErrNotFoundImageService)
			return
		}
		exception := errproc.NewException(ErrIntegrationsRedirect).WrapIntegrationError().CaptureSentry()
		exception.ConsolePrint()
		writeError(w, logger, http.StatusInternalServerError, ErrIntegrationsRedirect)
		return
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		o.logger.Errorf("Error reading response body: %v", err.Error())
		return
	}

	fmt.Println(string(bodyBytes))
	res := convert.ImageServiceUploadImageRes2Client(bodyBytes)

	writeJson(w, logger, res)
}
