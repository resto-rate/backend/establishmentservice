package models

type EstablishmentGetOneResponse struct {
	EstablishmentGUID  string           `json:"EstablishmentGUID"`
	EstablishmentTitle string           `json:"EstablishmentTitle"`
	Geolocation        Geolocation      `json:"Geolocation"`
	EstablishmentLogo  string           `json:"EstablishmentLogo"`
	Type               string           `json:"Type"`
	Cuisines           []string         `json:"Cuisines"`
	Images             []string         `json:"Images"`
	Rating             Rating           `json:"Rating"`
	AvgBill            float64          `json:"AvgBill"`
	Description        string           `json:"Description"`
	Address            string           `json:"Address"`
	Contacts           ContactsResponse `json:"Contacts"`
	Schedule           ScheduleResponse `json:"Schedule"`
	FavDishes          []FavDish        `json:"FavDishes"`
	AdditionalServices []string         `json:"AdditionalServices"`
	SuitableCases      []string         `json:"SuitableCases"`
	AdminGUIDs         []string         `json:"AdminGUIDs"`
	CollectionsGUIDs   []string         `json:"CollectionsGUIDs"`
}

type Rating struct {
	AvgRating   float64              `json:"AvgRating"`
	ReviewCount int64                `json:"ReviewCount"`
	AvgRates    RatingNestedAvgRates `json:"AvgRates"`
	Images      []string             `json:"Images"`
}

type RatingNestedAvgRates struct {
	Service      float64 `json:"Service"`
	Food         float64 `json:"Food"`
	Vibe         float64 `json:"Vibe"`
	PriceQuality float64 `json:"PriceQuality"`
	WaitingTime  float64 `json:"WaitingTime"`
}

type Geolocation struct {
	Latitude  float64 `json:"Latitude"`
	Longitude float64 `json:"Longitude"`
}

type FavDish struct {
	DishGUID          string  `json:"DishGUID"`
	EstablishmentGUID string  `json:"EstablishmentGUID"`
	Category          string  `json:"Category"`
	Weight            int64   `json:"Weight"`
	Volume            int64   `json:"Volume"`
	DishImage         string  `json:"DishImage"`
	DishName          string  `json:"DishName"`
	LikeCount         int64   `json:"LikeCount"`
	Composition       string  `json:"Composition"`
	Calories          float64 `json:"Calories"`
	Proteins          float64 `json:"Proteins"`
	Fats              float64 `json:"Fats"`
	Carbohydrates     float64 `json:"Carbohydrates"`
	Price             int64   `json:"Price"`
}

type ScheduleResponse struct {
	Monday    DayOfWeek `json:"Monday"`
	Tuesday   DayOfWeek `json:"Tuesday"`
	Wednesday DayOfWeek `json:"Wednesday"`
	Thursday  DayOfWeek `json:"Thursday"`
	Friday    DayOfWeek `json:"Friday"`
	Saturday  DayOfWeek `json:"Saturday"`
	Sunday    DayOfWeek `json:"Sunday"`
}

type ContactsResponse struct {
	MobilePhone string `json:"MobilePhone"`
	SiteURL     string `json:"SiteURL"`
	WhatsApp    string `json:"WhatsApp"`
	Telegram    string `json:"Telegram"`
	VK          string `json:"VK"`
}

type EstablishmentSearchArgs struct {
	Page                     int
	OrderBy                  string
	OrderDirection           int
	FullTextFind             string
	ItemPerPage              int
	RatingAboveFourStars     bool
	Types                    []string
	AvgBillFrom              *float64
	AvgBillTo                *float64
	AvailabilityOfPromotions bool
	SuitableCases            []string
	AdditionalServices       []string
	City                     *string
}

type EstablishmentGetInputAddressArgs struct {
	FullTextFind string
}

type EstablishmentsSearchResElem struct {
	EstablishmentGUID  string   `json:"EstablishmentGUID"`
	EstablishmentTitle string   `json:"EstablishmentTitle"`
	Type               string   `json:"Type"`
	Cuisines           []string `json:"Cuisines"`
	Image              string   `json:"Image"`
	AvgRating          float64  `json:"AvgRating"`
	ReviewCount        int64    `json:"ReviewCount"`
	AvgBill            float64  `json:"AvgBill"`
	CollectionGUIDs    []string `json:"CollectionGUIDs"`
}

type EstablishmentsGetCollectionNested struct {
	EstablishmentGUID  string   `json:"EstablishmentGUID"`
	EstablishmentTitle string   `json:"EstablishmentTitle"`
	Type               string   `json:"Type"`
	Cuisines           []string `json:"Cuisines"`
	Image              string   `json:"Image"`
	AvgRating          float64  `json:"AvgRating"`
	ReviewCount        int64    `json:"ReviewCount"`
	AvgBill            float64  `json:"AvgBill"`
}

type EstablishmentSearchRes struct {
	Count          int
	Establishments []EstablishmentsSearchResElem
}

type GetInputTypesRes struct {
	EstablishmentTypes []string `json:"EstablishmentTypes"`
	DishCategories     []string `json:"DishCategories"`
	CuisineTypes       []string `json:"CuisineTypes"`
	SuitableCases      []string `json:"SuitableCases"`
	AdditionalServices []string `json:"AdditionalServices"`
}

type GetInputAddressRes struct {
	Address []string `json:"Address"`
}

type CreateEstablishmentReq struct {
	EstablishmentGUID   string                         `json:"EstablishmentGUID"`
	EstablishmentTitle  string                         `json:"EstablishmentTitle"`
	EstablishmentStatus int                            `json:"EstablishmentStatus"`
	EstablishmentLogo   string                         `json:"EstablishmentLogo"`
	Address             string                         `json:"Address"`
	Type                string                         `json:"Type"`
	Cuisines            []string                       `json:"Cuisines"`
	Images              []string                       `json:"Images"`
	Description         string                         `json:"Description"`
	Contacts            CreateEstablishmentContactsReq `json:"Contacts"`
	Schedule            CreateEstablishmentScheduleReq `json:"Schedule"`
	SuitableCases       []string                       `json:"SuitableCases"`
	AdditionalServices  []string                       `json:"AdditionalServices"`
	AdminGUIDs          []string                       `json:"AdminGUIDs"`
}

type CreateEstablishmentContactsReq struct {
	MobilePhone string `json:"MobilePhone"`
	SiteURL     string `json:"SiteURL"`
	WhatsApp    string `json:"WhatsApp"`
	Telegram    string `json:"Telegram"`
	VK          string `json:"VK"`
}

type CreateEstablishmentScheduleReq struct {
	Monday    DayOfWeek `json:"Monday"`
	Tuesday   DayOfWeek `json:"Tuesday"`
	Wednesday DayOfWeek `json:"Wednesday"`
	Thursday  DayOfWeek `json:"Thursday"`
	Friday    DayOfWeek `json:"Friday"`
	Saturday  DayOfWeek `json:"Saturday"`
	Sunday    DayOfWeek `json:"Sunday"`
}

type DayOfWeek struct {
	OpenTime  int64 `json:"OpenTime"`
	CloseTime int64 `json:"CloseTime"`
	RestDay   bool  `json:"RestDay"`
}
