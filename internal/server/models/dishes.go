package models

type DishesGetAllResponse struct {
	Count            int                              `json:"Count"`
	DishesByCategory []DishesGetAllResponseByCategory `json:"DishesByCategory"`
}

type DishesGetAllResponseByCategory struct {
	Category string                     `json:"Category"`
	Dishes   []DishesGetAllResponseItem `json:"Dishes"`
}

type DishesGetAllResponseItem struct {
	DishGUID          string  `json:"DishGUID"`
	EstablishmentGUID string  `json:"EstablishmentGUID"`
	Category          string  `json:"Category"`
	Price             int64   `json:"Price"`
	Weight            int64   `json:"Weight"`
	Volume            int64   `json:"Volume"`
	DishImage         string  `json:"DishImage"`
	DishName          string  `json:"DishName"`
	LikeCount         int64   `json:"LikeCount"`
	Composition       string  `json:"Composition"`
	Calories          float64 `json:"Calories"`
	Proteins          float64 `json:"Proteins"`
	Fats              float64 `json:"Fats"`
	Carbohydrates     float64 `json:"Carbohydrates"`
}

type CreateDishReq struct {
	DishGUID      string  `json:"DishGUID"`
	Category      string  `json:"Category"`
	Price         int64   `json:"Price"`
	Weight        int64   `json:"Weight"`
	Volume        int64   `json:"Volume"`
	DishImage     string  `json:"DishImage"`
	DishName      string  `json:"DishName"`
	Composition   string  `json:"Composition"`
	Calories      float64 `json:"Calories"`
	Proteins      float64 `json:"Proteins"`
	Fats          float64 `json:"Fats"`
	Carbohydrates float64 `json:"Carbohydrates"`
}
