package models

type EventsCreateReq struct {
	EventGUID   string `json:"EventGUID"`
	Title       string `json:"Title"`
	Image       string `json:"Image"`
	Date        string `json:"Date"`
	DateTime    int64  `json:"DateTime"`
	Description string `json:"Description"`
	Timezone    int8   `json:"Timezone"`
	ContactInfo string `json:"ContactInfo"`
}

type EventsGetAllResponse struct {
	Count  int
	Events []EventsGetAllResponseItem
}

type EventsGetAllResponseItem struct {
	EventGUID          string `json:"EventGUID"`
	EstablishmentGUID  string `json:"EstablishmentGUID"`
	EstablishmentTitle string `json:"EstablishmentTitle"`
	Title              string `json:"Title"`
	Image              string `json:"Image"`
	Description        string `json:"Description"`
	DateTime           int64  `json:"DateTime"`
	ContactInfo        string `json:"ContactInfo"`
}

type EventsByCities struct {
	City string
}
