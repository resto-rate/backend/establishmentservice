package models

type ReviewsCreateReq struct {
	ReviewGUID        string                       `json:"ReviewGUID"`
	EstablishmentGUID string                       `json:"EstablishmentGUID"`
	ReviewerGUID      string                       `json:"ReviewerGUID"`
	LikedTheMost      string                       `json:"LikedTheMost"`
	NeedToBeChanged   string                       `json:"NeedToBeChanged"`
	Comment           string                       `json:"Comment"`
	Rating            ReviewsCreateReqNestedRating `json:"Rating"`
	Images            []string                     `json:"Images"`
}

type ReviewsCreateReqNestedRating struct {
	Service      int `json:"Service"`
	Food         int `json:"Food"`
	Vibe         int `json:"Vibe"`
	WaitingTime  int `json:"WaitingTime"`
	PriceQuality int `json:"PriceQuality"`
}

type ReviewsGetAllResponse struct {
	Count   int
	Reviews []ReviewsGetAllResponseItem
}

type ReviewsGetAllResponseItem struct {
	ReviewGUID      string                                       `json:"ReviewGUID"`
	Establishment   ReviewsGetAllResponseItemNestedEstablishment `json:"Establishment"`
	LikedTheMost    string                                       `json:"LikedTheMost"`
	NeedToBeChanged string                                       `json:"NeedToBeChanged"`
	Comment         string                                       `json:"Comment"`
	Rating          ReviewsCreateRespNestedRating                `json:"Rating"`
	Images          []string                                     `json:"Images"`
	Timestamp       int64                                        `json:"Timestamp"`
	User            ReviewsGetAllResponseItemNestedUser          `json:"User"`
}

type ReviewsGetAllResponseItemNestedEstablishment struct {
	EstablishmentGUID  string `json:"EstablishmentGUID"`
	EstablishmentImage string `json:"EstablishmentImage"`
	EstablishmentTitle string `json:"EstablishmentTitle"`
}

type ReviewsGetAllResponseItemNestedUser struct {
	AccountGUID string `json:"AccountGUID"`
	UserImage   string `json:"UserImage"`
	Name        string `json:"Name"`
	Surname     string `json:"Surname"`
}

type ReviewsCreateRespNestedRating struct {
	Service      int `json:"Service"`
	Food         int `json:"Food"`
	Vibe         int `json:"Vibe"`
	WaitingTime  int `json:"WaitingTime"`
	PriceQuality int `json:"PriceQuality"`
}
