package models

type PromotionCreateReq struct {
	PromotionGUID string `json:"PromotionGUID"`
	Title         string `json:"Title"`
	Image         string `json:"Image"`
	Conditions    string `json:"Conditions"`
	StartTime     int64  `json:"StartTime"`
	EndTime       int64  `json:"EndTime"`
	ContactInfo   string `json:"ContactInfo"`
	Undying       bool   `json:"Undying"`
}

type PromotionGetAllResponse struct {
	Count      int
	Promotions []PromotionGetAllResponseItem
}

type PromotionGetAllResponseItem struct {
	PromotionGUID     string `json:"PromotionGUID"`
	EstablishmentGUID string `json:"EstablishmentGUID"`
	Title             string `json:"Title"`
	Image             string `json:"Image"`
	Conditions        string `json:"Conditions"`
	StartTime         int64  `json:"StartTime"`
	EndTime           int64  `json:"EndTime"`
	ContactInfo       string `json:"ContactInfo"`
	Undying           bool   `json:"Undying"`
}
