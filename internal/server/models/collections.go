package models

type CollectionCreateReq struct {
	CollectionGUID    string   `json:"CollectionGUID"`
	AccountGUID       string   `json:"AccountGUID"`
	EstablishmentGUID []string `json:"EstablishmentGUID"`
	Image             string   `json:"Image"`
	Title             string   `json:"Title"`
}

type AddToCollectionReq struct {
	CollectionGUID    string `json:"CollectionGUID"`
	AccountGUID       string `json:"AccountGUID"`
	EstablishmentGUID string `json:"EstablishmentGUID"`
}

type CollectionGetAllResponse struct {
	Count       int                               `json:"Count"`
	Collections []*CollectionGetAllResponseNested `json:"Collections"`
}

type CollectionGetAllResponseNested struct {
	CollectionGUID     string `json:"CollectionGUID"`
	Preview            string `json:"Preview"`
	Title              string `json:"Title"`
	EstablishmentCount int    `json:"EstablishmentCount"`
	AccountGUID        string `json:"AccountGUID"`
}

type CollectionCreateResponse struct {
	CollectionGUID string   `json:"CollectionGUID"`
	AccountGUID    string   `json:"AccountGUID"`
	Establishments []string `json:"Establishment"`
	Title          string   `json:"Title"`
}

type CollectionGetAllResponseItem struct {
	CollectionGUID      string                               `json:"CollectionGUID"`
	AccountGUID         string                               `json:"AccountGUID"`
	Title               string                               `json:"Title"`
	Preview             string                               `json:"Preview"`
	EstablishmentsCount int                                  `json:"EstablishmentsCount"`
	Establishments      []*EstablishmentsGetCollectionNested `json:"Establishments"`
}
