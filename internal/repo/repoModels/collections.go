package repoModels

type CollectionsRepo interface {
	Upsert(collection *Collection) error
	Get(collectionGUID string) (*Collection, error)
	GetByAccountGUID(accountGUID string) CollectionsSlice
}

type CollectionGUID string
type CollectionsSlice []*Collection

type Collection struct {
	CollectionGUID     CollectionGUID `reindex:"CollectionGUID,hash,pk"`
	AccountGUID        string         `reindex:"AccountGUID,hash"`
	Title              string         `reindex:"Title,hash"`
	EstablishmentCount int            `reindex:"EstablishmentCount"`
	Preview            string         `reindex:"Preview"`
}

type CollectionNestedEstablishment struct {
	EstablishmentGUID  string `reindex:"EstablishmentGUID,hash"`
	EstablishmentTitle string `reindex:"EstablishmentTitle"`
	EstablishmentImage string `reindex:"EstablishmentImage"`
}
