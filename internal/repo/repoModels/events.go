package repoModels

type EventsRepo interface {
	Upsert(event *Event) error
	Delete(eventGUID EventGUID) error
	GetByEstablishmentGUID(establishmentGUID EstablishmentGUID) EventsSlice
	GetAllByCity(address string) EventsSlice
}

type EventGUID string
type EventsSlice []*Event

type Event struct {
	EventGUID          EventGUID         `reindex:"EventGUID,hash,pk"`
	EstablishmentGUID  EstablishmentGUID `reindex:"EstablishmentGUID,hash"`
	EstablishmentTitle string            `reindex:"EstablishmentTitle,hash"`
	Address            string            `reindex:"Address,hash"`
	Title              string            `reindex:"Title,hash"`
	Description        string            `reindex:"Description"`
	Image              string            `reindex:"Image"`
	Date               string            `reindex:"Date"`
	DateTime           int64             `reindex:"DateTime,ttl"`
	ContactInfo        string            `reindex:"ContactInfo"`
}
