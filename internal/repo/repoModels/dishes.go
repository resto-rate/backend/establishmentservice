package repoModels

var DishCategories []DishCategory

type DishCategory string

func init() {
	DishCategories = []DishCategory{
		"Супы", "Горячие блюда", "Холодные закуски", "Горячие закуски", "Салаты", "Супы", "Суши", "Роллы", "Пицца",
		"Гриль", "Стейки", "Морепродукты", "Паста", "Десерты", "Выпечка", "Чай", "Кофе", "Согревающие напитки",
		"Лимонады", "Соки", "Прохладительные напитки", "Алкогольные коктейли", "Безалкогольные коктейли", "Фреши",
		"Смузи", "Вино", "Крепкий алкоголь", "Пиво",
	}
}

type DishesRepo interface {
	GetAll(establishmentGUID EstablishmentGUID) (DishesMapByCategory, error)
	GetCategories() []DishCategory
	GetFavouritesByEstablishment(establishmentGUID EstablishmentGUID) (DishesSlice, error)
	Upsert(dish *Dish) error
	Delete(dishGUID string) error
	GetEstablishmentsForSearch(find string) []string
	/*GetByLogin(login string) (*Account, error)
	GetByAccountGUID(guid string) (*Account, error)
	Upsert(account *Account) error
	Auth(username string, password string) (*Account, error)
	GetToAccountGUID(AccountGUID) (*Account, error)
	UpsertAsync(users map[string]*Account) (err error)
	GetFromAccountGUID(accountGUIDs map[string]*string) map[string]*Account
	Search(arg *AccountsSearchArg) (*AccountSearchResult, error)
	Create(arg *AccountCreateArg) error
	Update(arg *Account) error
	GetAll() map[string]*AccountL
	GetAllForRole(role string) map[string]*Account*/
}

type DishGUID string
type DishesSlice []*Dish
type DishesMapByCategory map[DishCategory]DishesSlice

type Dish struct {
	DishGUID          string  `reindex:"DishGUID,hash,pk"`
	EstablishmentGUID string  `reindex:"EstablishmentGUID,hash"`
	Category          string  `reindex:"Category,hash"`
	Price             int64   `reindex:"Price,hash"`
	Weight            int64   `reindex:"Weight"`
	Volume            int64   `reindex:"Volume"`
	DishImage         string  `reindex:"DishImage"`
	DishName          string  `reindex:"DishName,hash"`
	LikeCount         int64   `reindex:"LikeCount,hash"`
	Composition       string  `reindex:"Composition,hash"`
	Calories          float64 `reindex:"Calories"`
	Proteins          float64 `reindex:"Proteins"`
	Fats              float64 `reindex:"Fats"`
	Carbohydrates     float64 `reindex:"Carbohydrates"`
}
