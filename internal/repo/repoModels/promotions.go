package repoModels

type PromotionsRepo interface {
	Upsert(promotion *Promotion) error
	Delete(promotionGUID PromotionGUID) error
	GetByEstablishmentGUID(estGUID EstablishmentGUID) PromotionsSlice
}

type PromotionGUID string
type PromotionsSlice []*Promotion

type Promotion struct {
	PromotionGUID     PromotionGUID     `reindex:"PromotionGUID,hash,pk"`
	EstablishmentGUID EstablishmentGUID `reindex:"EstablishmentGUID,hash"`
	Title             string            `reindex:"Title,hash"`
	Image             string            `reindex:"Image"`
	Conditions        string            `reindex:"Conditions"`
	StartTime         int64             `reindex:"StartTime"`
	EndTime           int64             `reindex:"EndTime,ttl"`
	ContactInfo       string            `reindex:"ContactInfo"`
	Undying           bool              `reindex:"Undying"`
}
