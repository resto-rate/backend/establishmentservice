package repoModels

type ReviewsRepo interface {
	Delete(reviewGUID ReviewGUID) error
	Upsert(review *Review) error
	GetByEstablishmentGUID(establishmentGUID EstablishmentGUID) ReviewsSlice
	GetByReviewerGUID(reviewerGUID string) ReviewsSlice
	GetByReviewGUID(reviewGUID string) (*Review, error)
}

type ReviewGUID string
type ReviewsSlice []*Review

type Review struct {
	ReviewGUID        ReviewGUID                   `reindex:"ReviewGUID,hash,pk"`
	EstablishmentGUID EstablishmentGUID            `reindex:"EstablishmentGUID,hash"`
	LikedTheMost      string                       `reindex:"LikedTheMost"`
	NeedToBeChanged   string                       `reindex:"NeedToBeChanged"`
	Comment           string                       `reindex:"Comment"`
	Rating            ReviewsCreateReqNestedRating `reindex:"Rating"`
	Images            []string                     `reindex:"Images"`
	Timestamp         int64                        `reindex:"TimeStamp"`
	ReviewerGUID      string                       `reindex:"ReviewerGUID"`
	User              ReviewsNestedUser            `reindex:"ReviewsNestedUser"`
}

type ReviewsNestedUser struct {
	AccountGUID string `reindex:"AccountGUID"`
	UserName    string `reindex:"UserName"`
	UserSurname string `reindex:"UserSurname"`
	UserImage   string `reindex:"UserImage"`
}

type ReviewsCreateReqNestedRating struct {
	Service      int `reindex:"Service"`
	Food         int `reindex:"Food"`
	Vibe         int `reindex:"Vibe"`
	WaitingTime  int `reindex:"WaitingTime"`
	PriceQuality int `reindex:"PriceQuality"`
}

type ReviewNestedAccount struct {
	AccountGUID string `reindex:"AccountGUID"`
	Name        string `reindex:"Name"`
	Surname     string `reindex:"Surname"`
	Image       string `reindex:"Image"`
}
