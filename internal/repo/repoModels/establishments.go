package repoModels

import "establishment-service/internal/integrations/authservice"

var EstablishmentTypes []EstablishmentType
var CuisinesTypes []CuisinesType
var SuitableCases []SuitableCase
var AdditionalServices []AdditionalService

type EstablishmentType string
type CuisinesType string
type SuitableCase string
type AdditionalService string

type EstablishmentStatus int64

var EstablishmentDeclineStatus EstablishmentStatus
var EstablishmentOnModerationStatus EstablishmentStatus
var EstablishmentAcceptStatus EstablishmentStatus

func init() {
	EstablishmentTypes = []EstablishmentType{
		"Бар", "Ресторан", "Кафе", "Кондитерская", "Доставка", "Кальян-бар", "Еда и напитки на вынос", "Фаст-фуд",
	}

	CuisinesTypes = []CuisinesType{
		"Итальянская", "Испанская", "Китайская", "Японская", "Корейская", "Французская", "Греческая", "Русская",
		"Мексиканская", "Украинская", "Белорусская", "Индийская", "Тайская", "Кавказская", "Узбекская", "Американская",
		"Чешская", "Вьетнамская", "Грузинская", "Израильская", "Армянская",
	}

	SuitableCases = []SuitableCase{
		"Романтический вечер", "Бизнес-встреча", "Семейный обед", "Для большой компании", "Банкеты", "Для фрилансеров",
		"Для завтраков",
	}

	AdditionalServices = []AdditionalService{
		"Бесплатный Wi-Fi", "Парковка для посетителей", "Детское меню", "Караоке", "Банкетный зал", "Детская комната",
		"Розетки для посетителей",
	}

	EstablishmentDeclineStatus = 2
	EstablishmentAcceptStatus = 1
	EstablishmentOnModerationStatus = 0
}

type EstablishmentsRepo interface {
	GetTypes() []EstablishmentType
	GetOne(establishmentGUID string) (*Establishment, error)
	Search(args *EstablishmentSearchArgs) (EstablishmentSlice, error)
	Upsert(establishment *Establishment) error
	GetCuisinesTypes() []CuisinesType
	GetAdditionalServices() []AdditionalService
	GetSuitableCases() []SuitableCase
	GetByAdminGUID(adminGUID string) (EstablishmentSlice, error)
	GetByEstablishmentGUIDs(guids []string) (EstablishmentSlice, error)
	GetByCollectionGUID(collectionGUID string) (EstablishmentSlice, error)
	Delete(establishmentGUID string) error
	/*GetByLogin(login string) (*Account, error)
	GetByAccountGUID(guid string) (*Account, error)
	Upsert(account *Account) error
	Auth(username string, password string) (*Account, error)
	GetToAccountGUID(AccountGUID) (*Account, error)
	UpsertAsync(users map[string]*Account) (err error)
	GetFromAccountGUID(accountGUIDs map[string]*string) map[string]*Account
	Search(arg *AccountsSearchArg) (*AccountSearchResult, error)
	Create(arg *AccountCreateArg) error
	Update(arg *Account) error
	GetAll() map[string]*Account
	GetAllForRole(role string) map[string]*Account*/
}

type EstablishmentGUID string
type EstablishmentSlice []*Establishment
type EstablishmentsByReviews map[string]*Establishment
type AccountsByReviews map[string]*authservice.Account

type Establishment struct {
	EstablishmentGUID   string            `reindex:"EstablishmentGUID,hash,pk"`
	EstablishmentTitle  string            `reindex:"EstablishmentTitle,hash"`
	EstablishmentLogo   string            `reindex:"EstablishmentLogo"`
	EstablishmentStatus int               `reindex:"EstablishmentStatus"`
	CityFiasID          string            `reindex:"CityFiasID,hash"`
	Geolocation         Geolocation       `reindex:"Geolocation"`
	Address             string            `reindex:"Address,hash"`
	Type                EstablishmentType `reindex:"Type,hash"`
	Cuisines            []string          `reindex:"Cuisines,hash"`
	Images              []string          `reindex:"Images"`
	AvgRating           float64           `reindex:"AvgRating"`
	ReviewCount         int64             `reindex:"ReviewCount"`
	AvgBill             float64           `reindex:"AvgBill,tree"`
	Description         string            `reindex:"Description"`
	Contacts            Contacts          `reindex:"Contacts"`
	Schedule            Schedule          `reindex:"Schedule"`
	AdditionalServices  []string          `reindex:"AdditionalServices,hash"`
	SuitableCases       []string          `reindex:"SuitableCases,hash"`
	AdminGUIDs          []string          `reindex:"AdminGUIDs,hash"`
	CollectionGUIDs     []string          `reindex:"CollectionGUIDs,hash"`
}

type Geolocation struct {
	Latitude  float64 `reindex:"latitude"`
	Longitude float64 `reindex:"longitude"`
}

type Schedule struct {
	Monday    DayOfWeek `reindex:"Monday"`
	Tuesday   DayOfWeek `reindex:"Tuesday"`
	Wednesday DayOfWeek `reindex:"Wednesday"`
	Thursday  DayOfWeek `reindex:"Thursday"`
	Friday    DayOfWeek `reindex:"Friday"`
	Saturday  DayOfWeek `reindex:"Saturday"`
	Sunday    DayOfWeek `reindex:"Sunday"`
}

type DayOfWeek struct {
	OpenTime  int64 `reindex:"OpenTime"`
	CloseTime int64 `reindex:"CloseTime"`
	RestDay   bool  `reindex:"RestDay"`
}

type Contacts struct {
	MobilePhone string `reindex:"MobilePhone"`
	SiteURL     string `reindex:"SiteURL"`
	WhatsApp    string `reindex:"WhatsApp"`
	Telegram    string `reindex:"Telegram"`
	VK          string `reindex:"VK"`
}

type EstablishmentSearchArgs struct {
	Page                     int
	ItemPerPage              int
	OrderBy                  string
	OrderDirection           int
	FullTextFind             string
	EstablishmentGUIDs       []string
	RatingAboveFourStars     bool
	Types                    []string
	AvgBillFrom              *float64
	AvgBillTo                *float64
	AvailabilityOfPromotions bool
	SuitableCases            []string
	AdditionalServices       []string
	City                     *string
}

type EstablishmentInputAddressArgs struct {
	FullTextFind string
}
