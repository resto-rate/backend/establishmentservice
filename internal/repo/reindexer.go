package repo

import (
	"errors"
	"establishment-service/internal/repo/repoModels"
	"fmt"
	"github.com/Restream/reindexer"
)

var (
	ErrorNotFound = errors.New("not found")
)

const (
	TableEstablishments = "establishments"
	TableDishes         = "dishes"
	TablePromotions     = "promotions"
	TableEvents         = "events"
	TableReviews        = "reviews"
	TableCollections    = "collections"
)

var Tables map[string]interface{}

func init() {
	Tables = make(map[string]interface{})
	Tables[TableEstablishments] = repoModels.Establishment{}
	Tables[TableDishes] = repoModels.Dish{}
	Tables[TablePromotions] = repoModels.Promotion{}
	Tables[TableEvents] = repoModels.Event{}
	Tables[TableReviews] = repoModels.Review{}
	Tables[TableCollections] = repoModels.Collection{}
}

func NewReindexerConnect(connect string) (*reindexer.Reindexer, error) {
	var err error

	// Подключение к БД
	reindexerConnect := reindexer.NewReindex(connect, reindexer.WithCreateDBIfMissing())

	// Проверяем состояние
	if reindexerConnect.Status().Err != nil {
		return nil, fmt.Errorf("NewDBConnect Status - (%w)", reindexerConnect.Status().Err)
	}

	// Производим открытие таблиц Reindexer
	for TableName, TableStruct := range Tables {
		err := reindexerConnect.OpenNamespace(TableName, reindexer.DefaultNamespaceOptions(), TableStruct)
		if err != nil {
			return nil, fmt.Errorf("NewDBConnect OpenNamespace - %s (%w)", TableName, err)
		}
	}

	// Возвращаем соединение
	return reindexerConnect, err
}

type Registry struct {
	repoModels.DishesRepo
	repoModels.EstablishmentsRepo
	repoModels.PromotionsRepo
	repoModels.EventsRepo
	repoModels.ReviewsRepo
	repoModels.CollectionsRepo
}
