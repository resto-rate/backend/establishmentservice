package repoFunctions

import (
	"establishment-service/internal/config"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"github.com/Restream/reindexer"
)

type CollectionsRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewCollectionsRepo(db *reindexer.Reindexer) *CollectionsRepo {
	return &CollectionsRepo{
		db: db,
	}
}

func (o *CollectionsRepo) Delete(collectionGUID repoModels.CollectionGUID) error {
	return o.db.Delete(repo.TableCollections, &repoModels.Collection{CollectionGUID: collectionGUID})
}

func (o *CollectionsRepo) Upsert(collection *repoModels.Collection) error {
	return o.db.Upsert(repo.TableCollections, collection)
}

func (o *CollectionsRepo) Get(collectionGUID string) (*repoModels.Collection, error) {
	elem, found := o.db.Query(repo.TableCollections).WhereString("CollectionGUID", reindexer.EQ, collectionGUID).Get()

	if found {
		model := elem.(*repoModels.Collection)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}

func (o *CollectionsRepo) GetByAccountGUID(accountGUID string) repoModels.CollectionsSlice {
	query := o.db.Query(repo.TableCollections)
	query.Where("AccountGUID", reindexer.EQ, accountGUID)

	iterator := query.Exec()

	result := repoModels.CollectionsSlice{}
	for iterator.Next() {
		result = append(result, iterator.Object().(*repoModels.Collection))
	}

	return result
}
