package repoFunctions

import (
	"establishment-service/internal/config"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"fmt"
	"github.com/Restream/reindexer"
)

type EventsRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewEventsRepo(db *reindexer.Reindexer) *EventsRepo {
	return &EventsRepo{
		db: db,
	}
}

func (o *EventsRepo) Delete(eventGUID repoModels.EventGUID) error {
	return o.db.Delete(repo.TableEvents, &repoModels.Event{EventGUID: eventGUID})
}

func (o *EventsRepo) Upsert(event *repoModels.Event) error {
	return o.db.Upsert(repo.TableEvents, event)
}

func (o *EventsRepo) GetByEstablishmentGUID(establishmentGUID repoModels.EstablishmentGUID) repoModels.EventsSlice {
	query := o.db.Query(repo.TableEvents)
	query.Where("EstablishmentGUID", reindexer.EQ, establishmentGUID)

	iterator := query.Exec()

	result := make(repoModels.EventsSlice, 0)
	for iterator.Next() {
		result = append(result, iterator.Object().(*repoModels.Event))
	}

	return result
}

func (o *EventsRepo) GetAllByCity(address string) repoModels.EventsSlice {
	query := o.db.Query(repo.TableEvents).WhereString("Address", reindexer.LIKE, fmt.Sprintf("%%%s%%", address))

	iterator := query.Exec()
	result := make(repoModels.EventsSlice, 0)
	for iterator.Next() {
		result = append(result, iterator.Object().(*repoModels.Event))
	}

	return result
}
