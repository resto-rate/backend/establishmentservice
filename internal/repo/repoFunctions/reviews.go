package repoFunctions

import (
	"establishment-service/internal/config"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"github.com/Restream/reindexer"
)

type ReviewsRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewReviewsRepo(db *reindexer.Reindexer) *ReviewsRepo {
	return &ReviewsRepo{
		db: db,
	}
}

func (o *ReviewsRepo) Delete(reviewGUID repoModels.ReviewGUID) error {
	return o.db.Delete(repo.TableReviews, &repoModels.Review{ReviewGUID: reviewGUID})
}

func (o *ReviewsRepo) Upsert(review *repoModels.Review) error {
	return o.db.Upsert(repo.TableReviews, review)
}

func (o *ReviewsRepo) GetByEstablishmentGUID(establishmentGUID repoModels.EstablishmentGUID) repoModels.ReviewsSlice {
	query := o.db.Query(repo.TableReviews)
	query.Where("EstablishmentGUID", reindexer.EQ, establishmentGUID)

	iterator := query.Exec()

	result := make(repoModels.ReviewsSlice, 0)
	for iterator.Next() {
		result = append(result, iterator.Object().(*repoModels.Review))
	}

	return result
}

func (o *ReviewsRepo) GetByReviewerGUID(reviewerGUID string) repoModels.ReviewsSlice {
	query := o.db.Query(repo.TableReviews)
	query.Where("ReviewerGUID", reindexer.EQ, reviewerGUID)

	iterator := query.Exec()

	result := repoModels.ReviewsSlice{}
	for iterator.Next() {
		result = append(result, iterator.Object().(*repoModels.Review))
	}

	return result
}

func (o *ReviewsRepo) GetByReviewGUID(reviewGUID string) (*repoModels.Review, error) {
	elem, found := o.db.Query(repo.TableReviews).WhereString("ReviewGUID", reindexer.EQ, reviewGUID).Get()

	if found {
		model := elem.(*repoModels.Review)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}
