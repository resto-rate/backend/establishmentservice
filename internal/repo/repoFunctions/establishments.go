package repoFunctions

import (
	"establishment-service/internal/config"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"fmt"
	"github.com/Restream/reindexer"
)

type EstablishmentsRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewEstablishmentsRepo(db *reindexer.Reindexer) *EstablishmentsRepo {
	return &EstablishmentsRepo{
		db: db,
	}
}

func (o *EstablishmentsRepo) GetOne(establishmentGUID string) (*repoModels.Establishment, error) {
	elem, found := o.db.Query(repo.TableEstablishments).WhereString("EstablishmentGUID", reindexer.EQ, establishmentGUID).Get()

	if found {
		model := elem.(*repoModels.Establishment)
		return model, nil
	}

	return nil, repo.ErrorNotFound
}

func (o *EstablishmentsRepo) Search(args *repoModels.EstablishmentSearchArgs) (repoModels.EstablishmentSlice, error) {
	query := o.db.Query(repo.TableEstablishments)

	if args.Page >= 0 {
		query.Limit(args.ItemPerPage).Offset(args.ItemPerPage * args.Page)
	} else {
		return nil, ErrorPageNumber
	}

	if args.FullTextFind != "" {
		query.WhereString("EstablishmentTitle", reindexer.LIKE, fmt.Sprintf("%%%s%%", args.FullTextFind)).
			Or().WhereString("Address", reindexer.LIKE, fmt.Sprintf("%%%s%%", args.FullTextFind)).
			Or().Where("Type", reindexer.LIKE, fmt.Sprintf("%%%s%%", args.FullTextFind)).
			Or().Where("Cuisines", reindexer.LIKE, fmt.Sprintf("%%%s%%", args.FullTextFind)).
			Or().WhereString("Description", reindexer.LIKE, fmt.Sprintf("%%%s%%", args.FullTextFind)).
			Or().Where("AdditionalServices", reindexer.LIKE, fmt.Sprintf("%%%s%%", args.FullTextFind)).
			Or().Where("SuitableCases", reindexer.LIKE, fmt.Sprintf("%%%s%%", args.FullTextFind)).
			Or().Where("EstablishmentGUID", reindexer.EQ, args.EstablishmentGUIDs)
	}

	if args.City != nil {
		query.WhereString("Address", reindexer.LIKE, fmt.Sprintf("%%%s%%", *args.City))
	}

	if args.AvgBillFrom != nil {
		query.Where("AvgBill", reindexer.GE, *args.AvgBillFrom)
	}
	if args.AvgBillTo != nil {
		query.Where("AvgBill", reindexer.LE, *args.AvgBillTo)
	}
	if args.RatingAboveFourStars == true {
		query.WhereDouble("AvgRating", reindexer.GE, 4)
	}
	if len(args.Types) > 0 {
		query.WhereString("Type", reindexer.EQ, args.Types...)
	}
	if len(args.SuitableCases) > 0 {
		query.WhereString("SuitableCases", reindexer.EQ, args.SuitableCases...)
	}
	if len(args.AdditionalServices) > 0 {
		query.WhereString("AdditionalServices", reindexer.EQ, args.AdditionalServices...)
	}

	iteratorEstablishments := query.Exec()

	result := make(repoModels.EstablishmentSlice, 0)
	for iteratorEstablishments.Next() {
		result = append(result, iteratorEstablishments.Object().(*repoModels.Establishment))
	}

	return result, nil
}

func (o *EstablishmentsRepo) GetTypes() []repoModels.EstablishmentType {
	return repoModels.EstablishmentTypes
}

func (o *EstablishmentsRepo) GetCuisinesTypes() []repoModels.CuisinesType {
	return repoModels.CuisinesTypes
}

func (o *EstablishmentsRepo) GetSuitableCases() []repoModels.SuitableCase {
	return repoModels.SuitableCases
}

func (o *EstablishmentsRepo) GetAdditionalServices() []repoModels.AdditionalService {
	return repoModels.AdditionalServices
}
func (o *EstablishmentsRepo) Upsert(establishment *repoModels.Establishment) error {
	return o.db.Upsert(repo.TableEstablishments, establishment)
}

func (o *EstablishmentsRepo) GetByAdminGUID(adminGUID string) (repoModels.EstablishmentSlice, error) {
	query := o.db.Query(repo.TableEstablishments).
		WhereString("AdminGUIDs", reindexer.EQ, adminGUID)

	iteratorEstablishments := query.Exec()

	result := make(repoModels.EstablishmentSlice, 0)
	for iteratorEstablishments.Next() {
		item, ok := iteratorEstablishments.Object().(*repoModels.Establishment)
		if !ok {
			return nil, fmt.Errorf("unable to convert type")
		}
		result = append(result, item)
	}

	return result, nil
}

func (o *EstablishmentsRepo) GetByEstablishmentGUIDs(guids []string) (repoModels.EstablishmentSlice, error) {
	query := o.db.Query(repo.TableEstablishments).
		WhereString("EstablishmentGUID", reindexer.EQ, guids...)

	iteratorEstablishments := query.Exec()

	result := make(repoModels.EstablishmentSlice, 0)
	for iteratorEstablishments.Next() {
		item, ok := iteratorEstablishments.Object().(*repoModels.Establishment)
		if !ok {
			return nil, fmt.Errorf("unable to convert type")
		}
		result = append(result, item)
	}

	return result, nil
}

func (o *EstablishmentsRepo) GetByCollectionGUID(collectionGUID string) (repoModels.EstablishmentSlice, error) {
	query := o.db.Query(repo.TableEstablishments).
		WhereString("CollectionGUIDs", reindexer.EQ, collectionGUID)

	iteratorEstablishments := query.Exec()

	result := make(repoModels.EstablishmentSlice, 0)
	for iteratorEstablishments.Next() {
		item, ok := iteratorEstablishments.Object().(*repoModels.Establishment)
		if !ok {
			return nil, fmt.Errorf("unable to convert type")
		}
		result = append(result, item)
	}

	return result, nil
}

func (o *EstablishmentsRepo) Delete(establishmentGUID string) error {
	return o.db.Delete(repo.TableEstablishments, &repoModels.Establishment{EstablishmentGUID: establishmentGUID})
}
