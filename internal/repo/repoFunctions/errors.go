package repoFunctions

import "errors"

var (
	ErrorAssociationType = errors.New("association type error")
	ErrorNoRemovedItems  = errors.New("no removed items")
	ErrorNotFound        = errors.New("not found")
	ErrorPageNumber      = errors.New("invalid page number requested")
	ErrorOrderDirection  = errors.New("invalid OrderDirection")
	ErrSessionExpired    = errors.New("session expired")
	ErrSessionNotFound   = errors.New("session not found")
	ErrorEmptyRequest    = errors.New("empty request")
	ErrorCoordinates     = errors.New("no coordinates passed for search")
	ErrorPreordersLen    = errors.New("len of preorders is 0")
	ErrorArgsAreEmpty    = errors.New("arguments are empty")
)
