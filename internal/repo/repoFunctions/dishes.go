package repoFunctions

import (
	"establishment-service/internal/config"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"fmt"
	"github.com/Restream/reindexer"
)

type DishesRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewDishesRepo(db *reindexer.Reindexer) *DishesRepo {
	return &DishesRepo{
		db: db,
	}
}

func (o *DishesRepo) GetAll(establishmentGUID repoModels.EstablishmentGUID) (repoModels.DishesMapByCategory, error) {
	query := o.db.Query(repo.TableDishes)
	query.Where("EstablishmentGUID", reindexer.EQ, establishmentGUID)

	iteratorEstablishments := query.Exec()

	result := make(repoModels.DishesMapByCategory)
	for iteratorEstablishments.Next() {
		dish := iteratorEstablishments.Object().(*repoModels.Dish)
		result[repoModels.DishCategory(dish.Category)] = append(result[repoModels.DishCategory(dish.Category)], dish)
	}

	return result, nil
}

func (o *DishesRepo) GetCategories() []repoModels.DishCategory {
	return repoModels.DishCategories
}

func (o *DishesRepo) GetFavouritesByEstablishment(establishmentGUID repoModels.EstablishmentGUID) (repoModels.DishesSlice, error) {
	query := o.db.Query(repo.TableDishes)
	query.Where("EstablishmentGUID", reindexer.EQ, establishmentGUID)
	query.Sort("LikeCount", true)
	query.Limit(4)

	iteratorEstablishments := query.Exec()

	result := make(repoModels.DishesSlice, 0)
	for iteratorEstablishments.Next() {
		result = append(result, iteratorEstablishments.Object().(*repoModels.Dish))
	}

	return result, nil
}

func (o *DishesRepo) Upsert(dish *repoModels.Dish) error {
	return o.db.Upsert(repo.TableDishes, dish)
}

func (o *DishesRepo) Delete(dishGUID string) error {
	return o.db.Delete(repo.TableDishes, &repoModels.Dish{DishGUID: dishGUID})
}

func (o *DishesRepo) GetEstablishmentsForSearch(find string) []string {
	query := o.db.Query(repo.TableDishes)
	result := make([]string, 0)

	if find != "" {
		query.WhereString("Category", reindexer.LIKE, fmt.Sprintf("%%%s%%", find)).
			Or().WhereString("DishName", reindexer.LIKE, fmt.Sprintf("%%%s%%", find)).
			Or().WhereString("Composition", reindexer.LIKE, fmt.Sprintf("%%%s%%", find))
	}

	dishes := query.Exec()
	for dishes.Next() {
		item := dishes.Object().(*repoModels.Dish)
		result = append(result, item.EstablishmentGUID)
	}

	return result
}
