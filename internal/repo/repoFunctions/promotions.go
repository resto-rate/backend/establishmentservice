package repoFunctions

import (
	"establishment-service/internal/config"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoModels"
	"github.com/Restream/reindexer"
)

type PromotionsRepo struct {
	db     *reindexer.Reindexer
	config *config.Config
}

func NewPromotionsRepo(db *reindexer.Reindexer) *PromotionsRepo {
	return &PromotionsRepo{
		db: db,
	}
}

func (o *PromotionsRepo) Delete(promotionGUID repoModels.PromotionGUID) error {
	return o.db.Delete(repo.TablePromotions, &repoModels.Promotion{PromotionGUID: promotionGUID})
}

func (o *PromotionsRepo) Upsert(promotion *repoModels.Promotion) error {
	return o.db.Upsert(repo.TablePromotions, promotion)
}

func (o *PromotionsRepo) GetByEstablishmentGUID(estGUID repoModels.EstablishmentGUID) repoModels.PromotionsSlice {
	query := o.db.Query(repo.TablePromotions)
	query.Where("EstablishmentGUID", reindexer.EQ, estGUID)

	iterator := query.Exec()

	result := make(repoModels.PromotionsSlice, 0)
	for iterator.Next() {
		result = append(result, iterator.Object().(*repoModels.Promotion))
	}

	return result
}
