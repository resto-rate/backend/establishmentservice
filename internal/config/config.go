package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"time"
)

const (
	configPath = "."
	configName = "conf"
)

type Config struct {
	Logger    logger    `yaml:"Logger"`
	WebServer webServer `yaml:"WebServer"`
	// Общие настройки
	FullTextItemCount          int // Количество выдаваемых вариантов полнотекстового поиска
	MinLenFindString           int // Минимальное количество символов для начала поиска
	ImageServiceAddress        string
	AuthServiceAddress         string
	NotificationServiceAddress string
}

func NewConfiguration() (*Config, error) {
	viper.AddConfigPath(configPath)
	viper.SetConfigName(configName)

	var configuration Config

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	err = viper.Unmarshal(&configuration)
	if err != nil {
		return nil, err
	}

	viper.SetEnvPrefix("establishmentservice")
	err = viper.BindEnv("port")
	if err != nil {
		return nil, err
	}

	configuration.WebServer.ListenPorts.Http = viper.GetString("port")

	err = viper.BindEnv("imageserviceaddress")
	if err != nil {
		return nil, err
	}
	configuration.ImageServiceAddress = viper.GetString("imageserviceaddress")

	err = viper.BindEnv("authserviceaddress")
	if err != nil {
		return nil, err
	}
	configuration.AuthServiceAddress = viper.GetString("authserviceaddress")

	err = viper.BindEnv("notificationserviceaddress")
	if err != nil {
		return nil, err
	}
	configuration.NotificationServiceAddress = viper.GetString("notificationserviceaddress")

	return &configuration, nil
}

type webServer struct {
	ListenPorts struct {
		Http string `yaml:"Http"`
	} `yaml:"ListenPorts"`
	Configuration struct {
		ReadTimeout       time.Duration `yaml:"ReadTimeout"`
		ReadHeaderTimeout time.Duration `yaml:"ReadHeaderTimeout"`
		WriteTimeout      time.Duration `yaml:"WriteTimeout"`
		IdleTimeout       time.Duration `yaml:"IdleTimeout"`
		MaxHeaderBytes    int           `yaml:"MaxHeaderBytes"`
		ShutdownTimeout   time.Duration `yaml:"ShutdownTimeout"`
	} `yaml:"Configuration"`
}

type authSettings struct {
	AccessTokenExpiration  uint32 `yaml:"AccessTokenExpiration"`
	RefreshTokenExpiration uint32 `yaml:"RefreshTokenExpiration"`
	OTP                    struct {
		OTPEnabled           bool  `yaml:"OTPEnabled"`
		MaxRemainingAttempts int64 `yaml:"MaxRemainingAttempts"`
		LogRetentionTime     int64 `yaml:"LogRetentionTime"`
		Auth                 struct {
			SendText       string `yaml:"SendText"`
			CodeFrom       int64  `yaml:"CodeFrom"`
			CodeTo         int64  `yaml:"CodeTo"`
			ExpiredSeconds int64  `yaml:"ExpiredSeconds"`
		} `yaml:"Auth"`
	} `yaml:"OTP"`
}

type logger struct {
	Level logrus.Level
	Type  string
}
