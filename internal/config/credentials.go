package config

import (
	"github.com/spf13/viper"
)

type Credentials struct {
	Reindexer string
	DaData    DaDataCred
}

type DaDataCred struct {
	ApiKey    string
	SecretKey string
}

func NewCredentials() (*Credentials, error) {
	var credentials Credentials

	err := viper.BindEnv("db")
	if err != nil {
		return nil, err
	}

	credentials.Reindexer = viper.GetString("db")

	viper.SetEnvPrefix("dadata")
	err = viper.BindEnv("api_key")
	if err != nil {
		return nil, err
	}
	credentials.DaData.ApiKey = viper.GetString("api_key")

	err = viper.BindEnv("secret_key")
	if err != nil {
		return nil, err
	}

	credentials.DaData.SecretKey = viper.GetString("secret_key")

	return &credentials, nil
}
