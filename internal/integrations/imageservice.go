package integrations

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"time"
)

const (
	imageUploadUrl = "/image/upload"
)

func RedirectUploadImageFile(url string, r *http.Request) (*http.Response, error) {

	file, h, err := r.FormFile("imageFile")
	if err != nil {
		return nil, ErrGetFormFile
	}
	defer func(file multipart.File) {
		err := file.Close()
		if err != nil {

		}
	}(file)

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	fw, err := writer.CreateFormFile("imageFile", h.Filename)
	if err != nil {
		return nil, ErrCreateFormFile
	}

	_, err = io.Copy(fw, file)
	if err != nil {
		return nil, ErrCopyFormFile
	}
	err = writer.Close()
	if err != nil {
		return nil, ErrCloseWriter
	}

	imageServiceUrl := fmt.Sprintf("%s%s", url, imageUploadUrl)
	fmt.Println(imageServiceUrl)
	req, err := http.NewRequest(http.MethodPost, imageServiceUrl, bytes.NewReader(body.Bytes()))
	if err != nil {
		return nil, ErrNewRequestToImageService
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())

	client := http.Client{Timeout: time.Second * 20}
	res, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("%v: %w", ErrDoRequestToImageService.Error(), err)
	}
	if res.StatusCode == http.StatusNotFound {
		return nil, fmt.Errorf("%v: %w", ErrNotFoundImageService.Error(), err)
	}

	return res, nil
}
