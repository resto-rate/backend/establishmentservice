package authservice

type Account struct {
	AccountGUID     string
	Status          int64
	Login           string
	Mobile          string
	Name            string
	Surname         string
	ImageURL        string
	MaxSessionCount int64
	Role            string
	TimeZone        int64
	Blocked         bool
	EnterType       string
	ChatID          string
	City            string
}

type GetUsersByAddress struct {
	Users []string `json:"Users"`
}
