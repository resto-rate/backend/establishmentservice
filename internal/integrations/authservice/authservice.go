package authservice

import (
	"establishment-service/internal/integrations"
	"fmt"
	"net/http"
	"time"
)

const (
	getProfileUrl   = "/auth/profile/data"
	setRestAdminURL = "/auth/makerestadmin/"
	getByAddress    = "/auth/getbyaddress"
)

func GetProfile(url string, r *http.Request) (*http.Response, error) {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s", url, getProfileUrl), nil)
	if err != nil {
		return nil, integrations.ErrNewRequestToIAuthService
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", r.Header.Get("Authorization"))

	client := http.Client{Timeout: time.Second * 20}
	res, err := client.Do(req)
	if err != nil {
		return nil, integrations.ErrDoRequestToAuthService
	}

	return res, nil
}

func SetRestAdmin(url, adminGUID string) error {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s%s", url, setRestAdminURL, adminGUID), nil)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{Timeout: time.Second * 20}
	_, err = client.Do(req)
	if err != nil {
		return err
	}

	return nil
}

func GetByAddress(url, address string) (*http.Response, error) {
	temp := fmt.Sprintf("%s%s", url, getByAddress)
	req, err := http.NewRequest(http.MethodGet, temp, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	q := req.URL.Query()
	q.Add("city", address)
	req.URL.RawQuery = q.Encode()

	client := http.Client{Timeout: time.Second * 20}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, integrations.ErrDoRequestToAuthService
	}

	return resp, nil
}
