package notificationservice

import (
	"bytes"
	"encoding/json"
	"establishment-service/internal/integrations"
	"fmt"
	"net/http"
	"time"
)

const (
	sendNotification = "/sendNotification"
)

func SendNotification(url string, model Notification) (*http.Response, error) {
	body, err := json.Marshal(model)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s%s", url, sendNotification), bytes.NewBuffer(body))
	if err != nil {
		return nil, integrations.ErrNewRequestToIAuthService
	}
	req.Header.Set("Content-Type", "application/json")

	client := http.Client{Timeout: time.Second * 20}
	res, err := client.Do(req)
	if err != nil {
		return nil, integrations.ErrDoRequestToAuthService
	}

	return res, nil
}
