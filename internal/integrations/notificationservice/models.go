package notificationservice

type Notification struct {
	Message NotificationNestedMessage `json:"Message"`
	Users   []string                  `json:"Users"`
}

type NotificationNestedMessage struct {
	Title string `json:"Title"`
	Body  string `json:"Body"`
	Image string `json:"Image"`
}
