package integrations

import "errors"

var (
	ErrGetFormFile              = errors.New("failed to get form file from request")
	ErrCreateFormFile           = errors.New("failed to create from file")
	ErrCopyFormFile             = errors.New("failed to copy form file")
	ErrCloseWriter              = errors.New("failed to close writer")
	ErrNewRequestToImageService = errors.New("failed to create request to imageservice")
	ErrNewRequestToIAuthService = errors.New("failed to create request to authservice")
	ErrDoRequestToAuthService   = errors.New("failed to do request to authservice")
	ErrDoRequestToImageService  = errors.New("failed to do request to imageservice")
	ErrNotFoundImageService     = errors.New("image service not found")
)
