package convert

import (
	"crypto/sha256"
	"encoding/hex"
	"establishment-service/internal/integrations/authservice"
	"establishment-service/internal/repo/repoModels"
	"establishment-service/internal/server/models"
	"fmt"
	"github.com/ekomobile/dadata/v2/api/suggest"
	"github.com/google/uuid"
	"strconv"
	"time"
)

func EstablishmentSearchClientReq2Repo(o *models.EstablishmentSearchArgs) *repoModels.EstablishmentSearchArgs {

	return &repoModels.EstablishmentSearchArgs{
		Page:                     o.Page,
		ItemPerPage:              o.ItemPerPage,
		OrderBy:                  o.OrderBy,
		OrderDirection:           o.OrderDirection,
		FullTextFind:             o.FullTextFind,
		EstablishmentGUIDs:       nil,
		RatingAboveFourStars:     o.RatingAboveFourStars,
		Types:                    o.Types,
		AvgBillFrom:              o.AvgBillFrom,
		AvgBillTo:                o.AvgBillTo,
		AvailabilityOfPromotions: o.AvailabilityOfPromotions,
		SuitableCases:            o.SuitableCases,
		AdditionalServices:       o.AdditionalServices,
		City:                     o.City,
	}
}

func EstablishmentCreate2Repo(o *models.CreateEstablishmentReq, addresses []*suggest.AddressSuggestion) (*repoModels.Establishment, error) {

	if o.EstablishmentGUID == "" {
		uuid, err := uuid.NewUUID()
		if err != nil {
			return nil, err
		}
		o.EstablishmentGUID = uuid.String()
	}

	if addresses == nil {
		return nil, fmt.Errorf("addresses is empty")
	}
	address := addresses[0]
	lat, err := strconv.ParseFloat(address.Data.GeoLat, 64)
	if err != nil {
		return nil, err
	}
	lon, err := strconv.ParseFloat(address.Data.GeoLon, 64)
	if err != nil {
		return nil, err
	}

	return &repoModels.Establishment{
		EstablishmentGUID:   o.EstablishmentGUID,
		EstablishmentTitle:  o.EstablishmentTitle,
		EstablishmentLogo:   o.EstablishmentLogo,
		EstablishmentStatus: o.EstablishmentStatus,
		CityFiasID:          address.Data.CityFiasID,
		Geolocation: repoModels.Geolocation{
			Latitude:  lat,
			Longitude: lon,
		},
		Address:     o.Address,
		Type:        repoModels.EstablishmentType(o.Type),
		Cuisines:    o.Cuisines,
		Images:      o.Images,
		AvgRating:   0,
		ReviewCount: 0,
		AvgBill:     0,
		Description: o.Description,
		Contacts: repoModels.Contacts{
			MobilePhone: o.Contacts.MobilePhone,
			SiteURL:     o.Contacts.SiteURL,
			WhatsApp:    o.Contacts.WhatsApp,
			Telegram:    o.Contacts.Telegram,
			VK:          o.Contacts.VK,
		},
		Schedule: repoModels.Schedule{
			Monday: repoModels.DayOfWeek{
				OpenTime:  o.Schedule.Monday.OpenTime,
				CloseTime: o.Schedule.Monday.CloseTime,
				RestDay:   o.Schedule.Monday.RestDay,
			},
			Tuesday: repoModels.DayOfWeek{
				OpenTime:  o.Schedule.Tuesday.OpenTime,
				CloseTime: o.Schedule.Tuesday.CloseTime,
				RestDay:   o.Schedule.Tuesday.RestDay,
			},
			Wednesday: repoModels.DayOfWeek{
				OpenTime:  o.Schedule.Wednesday.OpenTime,
				CloseTime: o.Schedule.Wednesday.CloseTime,
				RestDay:   o.Schedule.Wednesday.RestDay,
			},
			Thursday: repoModels.DayOfWeek{
				OpenTime:  o.Schedule.Thursday.OpenTime,
				CloseTime: o.Schedule.Thursday.CloseTime,
				RestDay:   o.Schedule.Thursday.RestDay,
			},
			Friday: repoModels.DayOfWeek{
				OpenTime:  o.Schedule.Friday.OpenTime,
				CloseTime: o.Schedule.Friday.CloseTime,
				RestDay:   o.Schedule.Friday.RestDay,
			},
			Saturday: repoModels.DayOfWeek{
				OpenTime:  o.Schedule.Saturday.OpenTime,
				CloseTime: o.Schedule.Saturday.CloseTime,
				RestDay:   o.Schedule.Saturday.RestDay,
			},
			Sunday: repoModels.DayOfWeek{
				OpenTime:  o.Schedule.Sunday.OpenTime,
				CloseTime: o.Schedule.Sunday.CloseTime,
				RestDay:   o.Schedule.Sunday.RestDay,
			},
		},
		AdditionalServices: o.AdditionalServices,
		SuitableCases:      o.SuitableCases,
		AdminGUIDs:         o.AdminGUIDs,
	}, nil
}

func DishCreate2Repo(dish *models.CreateDishReq, estGUID string) (*repoModels.Dish, error) {

	if dish.DishGUID == "" {
		dishGUID, err := uuid.NewUUID()
		if err != nil {
			return nil, err
		}
		dish.DishGUID = dishGUID.String()
	}

	return &repoModels.Dish{
		DishGUID:          dish.DishGUID,
		EstablishmentGUID: estGUID,
		Category:          dish.Category,
		Price:             dish.Price,
		Weight:            dish.Weight,
		Volume:            dish.Volume,
		DishImage:         dish.DishImage,
		DishName:          dish.DishName,
		Composition:       dish.Composition,
		Calories:          dish.Calories,
		Proteins:          dish.Proteins,
		Fats:              dish.Fats,
		Carbohydrates:     dish.Carbohydrates,
	}, nil
}

func PromotionCreate2Repo(promotion *models.PromotionCreateReq, estGUID string) (*repoModels.Promotion, error) {

	if promotion.PromotionGUID == "" {
		dishGUID, err := uuid.NewUUID()
		if err != nil {
			return nil, err
		}
		promotion.PromotionGUID = dishGUID.String()
	}

	if promotion.EndTime == 0 && !promotion.Undying {
		return nil, fmt.Errorf("EndTime is empty")
	}

	var endTime int64
	if promotion.Undying {
		endTime = 1843545124
	} else {
		endTime = promotion.EndTime
	}

	return &repoModels.Promotion{
		PromotionGUID:     repoModels.PromotionGUID(promotion.PromotionGUID),
		EstablishmentGUID: repoModels.EstablishmentGUID(estGUID),
		Title:             promotion.Title,
		Image:             promotion.Image,
		Conditions:        promotion.Conditions,
		StartTime:         promotion.StartTime,
		EndTime:           endTime,
		ContactInfo:       promotion.ContactInfo,
		Undying:           promotion.Undying,
	}, nil
}

func EventCreate2Repo(event *models.EventsCreateReq, est *repoModels.Establishment) (*repoModels.Event, error) {

	if event.EventGUID == "" {
		eventGUID, err := uuid.NewUUID()
		if err != nil {
			return nil, err
		}
		event.EventGUID = eventGUID.String()
	}

	return &repoModels.Event{
		EventGUID:          repoModels.EventGUID(event.EventGUID),
		EstablishmentGUID:  repoModels.EstablishmentGUID(est.EstablishmentGUID),
		EstablishmentTitle: est.EstablishmentTitle,
		Address:            est.Address,
		Title:              event.Title,
		Description:        event.Description,
		Image:              event.Image,
		Date:               event.Date,
		DateTime:           event.DateTime,
		ContactInfo:        event.ContactInfo,
	}, nil
}

func ReviewCreateReq2Repo(review *models.ReviewsCreateReq, estGUID string, account authservice.Account) (*repoModels.Review, error) {

	tempGUID := fmt.Sprintf("%s%s", estGUID, account.AccountGUID)
	hash := sha256.New()
	hash.Write([]byte(tempGUID))
	hashBytes := hash.Sum(nil)

	return &repoModels.Review{
		ReviewGUID:        repoModels.ReviewGUID(hex.EncodeToString(hashBytes)),
		EstablishmentGUID: repoModels.EstablishmentGUID(estGUID),
		LikedTheMost:      review.LikedTheMost,
		NeedToBeChanged:   review.NeedToBeChanged,
		Comment:           review.Comment,
		Rating: repoModels.ReviewsCreateReqNestedRating{
			Service:      review.Rating.Service,
			Food:         review.Rating.Food,
			Vibe:         review.Rating.Vibe,
			WaitingTime:  review.Rating.WaitingTime,
			PriceQuality: review.Rating.PriceQuality,
		},
		Images:       review.Images,
		Timestamp:    time.Now().Unix(),
		ReviewerGUID: account.AccountGUID,
		User: repoModels.ReviewsNestedUser{
			AccountGUID: account.AccountGUID,
			UserName:    account.Name,
			UserSurname: account.Surname,
			UserImage:   account.ImageURL,
		},
	}, nil
}

func CollectionCreateReq2Repo(collection *models.CollectionCreateReq) (*repoModels.Collection, error) {
	if collection.CollectionGUID == "" {
		uuid, err := uuid.NewUUID()
		if err != nil {
			return nil, err
		}
		collection.CollectionGUID = uuid.String()
	}

	return &repoModels.Collection{
		CollectionGUID: repoModels.CollectionGUID(collection.CollectionGUID),
		AccountGUID:    collection.AccountGUID,
		Title:          collection.Title,
	}, nil
}
