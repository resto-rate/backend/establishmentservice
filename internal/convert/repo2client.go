package convert

import (
	"establishment-service/internal/repo/repoModels"
	"establishment-service/internal/server/models"
	"github.com/ekomobile/dadata/v2/api/suggest"
)

func EstablishmentGetOne2Client(establishment *repoModels.Establishment, favDishes repoModels.DishesSlice, reviews repoModels.ReviewsSlice) *models.EstablishmentGetOneResponse {
	var avgService, avgFood, avgVibe, avgPriceQuality, avgWaitingTime float64
	reviewsImages := make([]string, 0)
	for _, v := range reviews {
		avgService += float64(v.Rating.Service)
		avgFood += float64(v.Rating.Food)
		avgVibe += float64(v.Rating.Vibe)
		avgPriceQuality += float64(v.Rating.PriceQuality)
		avgWaitingTime += float64(v.Rating.WaitingTime)
		reviewsImages = append(reviewsImages, v.Images...)
	}

	if reviews != nil && len(reviews) > 0 {
		avgService /= float64(len(reviews))
		avgFood /= float64(len(reviews))
		avgVibe /= float64(len(reviews))
		avgPriceQuality /= float64(len(reviews))
		avgWaitingTime /= float64(len(reviews))
	}

	contacts := models.ContactsResponse{
		MobilePhone: establishment.Contacts.MobilePhone,
		SiteURL:     establishment.Contacts.SiteURL,
		WhatsApp:    establishment.Contacts.WhatsApp,
		Telegram:    establishment.Contacts.Telegram,
		VK:          establishment.Contacts.VK,
	}

	schedule := models.ScheduleResponse{
		Monday: models.DayOfWeek{
			OpenTime:  establishment.Schedule.Monday.OpenTime,
			CloseTime: establishment.Schedule.Monday.CloseTime,
			RestDay:   establishment.Schedule.Monday.RestDay,
		},
		Tuesday: models.DayOfWeek{
			OpenTime:  establishment.Schedule.Tuesday.OpenTime,
			CloseTime: establishment.Schedule.Tuesday.CloseTime,
			RestDay:   establishment.Schedule.Tuesday.RestDay,
		},
		Wednesday: models.DayOfWeek{
			OpenTime:  establishment.Schedule.Wednesday.OpenTime,
			CloseTime: establishment.Schedule.Wednesday.CloseTime,
			RestDay:   establishment.Schedule.Wednesday.RestDay,
		},
		Thursday: models.DayOfWeek{
			OpenTime:  establishment.Schedule.Thursday.OpenTime,
			CloseTime: establishment.Schedule.Thursday.CloseTime,
			RestDay:   establishment.Schedule.Thursday.RestDay,
		},
		Friday: models.DayOfWeek{
			OpenTime:  establishment.Schedule.Friday.OpenTime,
			CloseTime: establishment.Schedule.Friday.CloseTime,
			RestDay:   establishment.Schedule.Friday.RestDay,
		},
		Saturday: models.DayOfWeek{
			OpenTime:  establishment.Schedule.Saturday.OpenTime,
			CloseTime: establishment.Schedule.Saturday.CloseTime,
			RestDay:   establishment.Schedule.Saturday.RestDay,
		},
		Sunday: models.DayOfWeek{
			OpenTime:  establishment.Schedule.Sunday.OpenTime,
			CloseTime: establishment.Schedule.Sunday.CloseTime,
			RestDay:   establishment.Schedule.Sunday.RestDay,
		},
	}

	dishes := make([]models.FavDish, 0, 4)
	for _, v := range favDishes {
		dishes = append(dishes, models.FavDish{
			DishGUID:          v.DishGUID,
			EstablishmentGUID: v.EstablishmentGUID,
			Category:          v.Category,
			Weight:            v.Weight,
			Volume:            v.Volume,
			DishImage:         v.DishImage,
			DishName:          v.DishName,
			LikeCount:         v.LikeCount,
			Composition:       v.Composition,
			Calories:          v.Calories,
			Proteins:          v.Proteins,
			Fats:              v.Fats,
			Carbohydrates:     v.Carbohydrates,
			Price:             v.Price,
		})
	}

	return &models.EstablishmentGetOneResponse{
		EstablishmentGUID:  establishment.EstablishmentGUID,
		EstablishmentTitle: establishment.EstablishmentTitle,
		Geolocation: models.Geolocation{
			Latitude:  establishment.Geolocation.Latitude,
			Longitude: establishment.Geolocation.Longitude,
		},
		EstablishmentLogo: establishment.EstablishmentLogo,
		Type:              string(establishment.Type),
		Cuisines:          establishment.Cuisines,
		Images:            establishment.Images,
		Rating: models.Rating{
			AvgRating:   establishment.AvgRating,
			ReviewCount: establishment.ReviewCount,
			AvgRates: models.RatingNestedAvgRates{
				Service:      avgService,
				Food:         avgFood,
				Vibe:         avgVibe,
				PriceQuality: avgPriceQuality,
				WaitingTime:  avgWaitingTime,
			},
			Images: reviewsImages,
		},
		AvgBill:            establishment.AvgBill,
		Description:        establishment.Description,
		Address:            establishment.Address,
		Contacts:           contacts,
		Schedule:           schedule,
		FavDishes:          dishes,
		AdditionalServices: establishment.AdditionalServices,
		SuitableCases:      establishment.SuitableCases,
		AdminGUIDs:         establishment.AdminGUIDs,
		CollectionsGUIDs:   establishment.CollectionGUIDs,
	}
}

func EstablishmentsSearchRepo2Client(r *repoModels.EstablishmentSlice) *models.EstablishmentSearchRes {

	result := &models.EstablishmentSearchRes{}
	result.Establishments = make([]models.EstablishmentsSearchResElem, 0)

	for _, p := range *r {
		var image string
		if len(p.Images) > 0 {
			image = p.Images[0]
		}

		result.Establishments = append(result.Establishments, models.EstablishmentsSearchResElem{
			EstablishmentGUID:  p.EstablishmentGUID,
			EstablishmentTitle: p.EstablishmentTitle,
			Type:               string(p.Type),
			Cuisines:           p.Cuisines,
			Image:              image,
			AvgRating:          p.AvgRating,
			ReviewCount:        p.ReviewCount,
			AvgBill:            p.AvgBill,
			CollectionGUIDs:    p.CollectionGUIDs,
		})
	}

	result.Count = len(*r)

	return result
}

func EstablishmentsByGetCollection2Client(r repoModels.EstablishmentSlice) []*models.EstablishmentsGetCollectionNested {
	result := make([]*models.EstablishmentsGetCollectionNested, 0)

	for _, p := range r {
		var image string
		if len(p.Images) > 0 {
			image = p.Images[0]
		}

		result = append(result, &models.EstablishmentsGetCollectionNested{
			EstablishmentGUID:  p.EstablishmentGUID,
			EstablishmentTitle: p.EstablishmentTitle,
			Type:               string(p.Type),
			Cuisines:           p.Cuisines,
			Image:              image,
			AvgRating:          p.AvgRating,
			ReviewCount:        p.ReviewCount,
			AvgBill:            p.AvgBill,
		})
	}

	return result
}

func EstablishmentGetInputAddress2Client(a []*suggest.AddressSuggestion) *models.GetInputAddressRes {
	result := &models.GetInputAddressRes{}
	result.Address = make([]string, 0, len(a))

	for _, p := range a {
		result.Address = append(result.Address, p.Value)
	}

	return result
}

func ImageServiceUploadImageRes2Client(bodyBytes []byte) string {
	if len(bodyBytes) > 0 && bodyBytes[0] == '"' {
		bodyBytes = bodyBytes[1:]
	}

	if len(bodyBytes) > 0 && bodyBytes[len(bodyBytes)-1] == '"' {
		bodyBytes = bodyBytes[:len(bodyBytes)-1]
	}

	return string(bodyBytes)
}

func DishesGetAll2Client(m repoModels.DishesMapByCategory) *models.DishesGetAllResponse {

	result := &models.DishesGetAllResponse{
		Count:            0,
		DishesByCategory: []models.DishesGetAllResponseByCategory{},
	}

	for k, p := range m {
		dishes := make([]models.DishesGetAllResponseItem, 0)
		for _, v := range p {
			dishes = append(dishes, models.DishesGetAllResponseItem{
				DishGUID:          v.DishGUID,
				EstablishmentGUID: v.EstablishmentGUID,
				Category:          v.Category,
				Price:             v.Price,
				Weight:            v.Weight,
				Volume:            v.Volume,
				DishImage:         v.DishImage,
				DishName:          v.DishName,
				LikeCount:         v.LikeCount,
				Composition:       v.Composition,
				Calories:          v.Calories,
				Proteins:          v.Proteins,
				Fats:              v.Fats,
				Carbohydrates:     v.Carbohydrates,
			})
		}
		result.DishesByCategory = append(result.DishesByCategory, models.DishesGetAllResponseByCategory{
			Category: string(k),
			Dishes:   dishes,
		})
	}

	result.Count = len(m)

	return result
}

func DishGetOne2Client(dish *repoModels.Dish) *models.DishesGetAllResponseItem {
	return &models.DishesGetAllResponseItem{
		DishGUID:          dish.DishGUID,
		EstablishmentGUID: dish.EstablishmentGUID,
		Category:          dish.Category,
		Price:             dish.Price,
		Weight:            dish.Weight,
		Volume:            dish.Volume,
		DishImage:         dish.DishImage,
		DishName:          dish.DishName,
		LikeCount:         dish.LikeCount,
		Composition:       dish.Composition,
		Calories:          dish.Calories,
		Proteins:          dish.Proteins,
		Fats:              dish.Fats,
		Carbohydrates:     dish.Carbohydrates,
	}
}

func PromotionGetOne2Client(p *repoModels.Promotion) *models.PromotionGetAllResponseItem {
	return &models.PromotionGetAllResponseItem{
		PromotionGUID:     string(p.PromotionGUID),
		EstablishmentGUID: string(p.EstablishmentGUID),
		Title:             p.Title,
		Image:             p.Image,
		Conditions:        p.Conditions,
		StartTime:         p.StartTime,
		EndTime:           p.EndTime,
		ContactInfo:       p.ContactInfo,
		Undying:           p.Undying,
	}
}

func EventGetOne2Client(p *repoModels.Event) *models.EventsGetAllResponseItem {
	return &models.EventsGetAllResponseItem{
		EventGUID:          string(p.EventGUID),
		EstablishmentGUID:  string(p.EstablishmentGUID),
		EstablishmentTitle: p.EstablishmentTitle,
		Title:              p.Title,
		Image:              p.Image,
		Description:        p.Description,
		DateTime:           p.DateTime,
		ContactInfo:        p.ContactInfo,
	}
}

func ReviewGetOne2Client(p *repoModels.Review, e *repoModels.Establishment) *models.ReviewsGetAllResponseItem {
	return &models.ReviewsGetAllResponseItem{
		ReviewGUID: string(p.ReviewGUID),
		Establishment: models.ReviewsGetAllResponseItemNestedEstablishment{
			EstablishmentGUID:  e.EstablishmentGUID,
			EstablishmentImage: e.EstablishmentLogo,
			EstablishmentTitle: e.EstablishmentTitle,
		},
		User: models.ReviewsGetAllResponseItemNestedUser{
			AccountGUID: p.User.AccountGUID,
			UserImage:   p.User.UserImage,
			Name:        p.User.UserName,
			Surname:     p.User.UserSurname,
		},
		Comment: p.Comment,
		Rating: models.ReviewsCreateRespNestedRating{
			Service:      p.Rating.Service,
			Food:         p.Rating.Food,
			Vibe:         p.Rating.Vibe,
			PriceQuality: p.Rating.PriceQuality,
			WaitingTime:  p.Rating.WaitingTime,
		},
		Images:    p.Images,
		Timestamp: p.Timestamp,
	}
}

func CollectionOne2Client(p *repoModels.Collection, establishments []*models.EstablishmentsGetCollectionNested) *models.CollectionGetAllResponseItem {
	return &models.CollectionGetAllResponseItem{
		CollectionGUID:      string(p.CollectionGUID),
		AccountGUID:         p.AccountGUID,
		Title:               p.Title,
		Preview:             p.Preview,
		EstablishmentsCount: len(establishments),
		Establishments:      establishments,
	}
}

func CollectionsRepo2Client(c repoModels.CollectionsSlice) models.CollectionGetAllResponse {
	res := models.CollectionGetAllResponse{}
	res.Collections = make([]*models.CollectionGetAllResponseNested, 0)
	for _, v := range c {
		res.Collections = append(res.Collections, &models.CollectionGetAllResponseNested{
			CollectionGUID:     string(v.CollectionGUID),
			Preview:            v.Preview,
			Title:              v.Title,
			EstablishmentCount: v.EstablishmentCount,
			AccountGUID:        v.AccountGUID,
		})
	}
	res.Count = len(c)
	return res
}

func PromotionGetAll2Client(p repoModels.PromotionsSlice) *models.PromotionGetAllResponse {
	res := &models.PromotionGetAllResponse{}
	res.Promotions = make([]models.PromotionGetAllResponseItem, 0, len(p))

	for _, v := range p {
		res.Promotions = append(res.Promotions, models.PromotionGetAllResponseItem{
			PromotionGUID:     string(v.PromotionGUID),
			EstablishmentGUID: string(v.EstablishmentGUID),
			Title:             v.Title,
			Image:             v.Image,
			Conditions:        v.Conditions,
			StartTime:         v.StartTime,
			EndTime:           v.EndTime,
			ContactInfo:       v.ContactInfo,
			Undying:           v.Undying,
		})
	}

	res.Count = len(p)

	return res
}

func EventGetAll2Client(p repoModels.EventsSlice) *models.EventsGetAllResponse {
	res := &models.EventsGetAllResponse{}
	res.Events = make([]models.EventsGetAllResponseItem, 0, len(p))

	for _, v := range p {
		res.Events = append(res.Events, models.EventsGetAllResponseItem{
			EventGUID:          string(v.EventGUID),
			EstablishmentGUID:  string(v.EstablishmentGUID),
			EstablishmentTitle: v.EstablishmentTitle,
			Title:              v.Title,
			Image:              v.Image,
			Description:        v.Description,
			DateTime:           v.DateTime,
			ContactInfo:        v.ContactInfo,
		})
	}

	res.Count = len(p)

	return res
}

type InputTypesGetAllArgs struct {
	EstTypes           []repoModels.EstablishmentType
	DishCategories     []repoModels.DishCategory
	CuisineTypes       []repoModels.CuisinesType
	SuitableCases      []repoModels.SuitableCase
	AdditionalServices []repoModels.AdditionalService
}

func InputTypesGetAll2Client(args InputTypesGetAllArgs) models.GetInputTypesRes {
	res := models.GetInputTypesRes{}

	for _, v := range args.EstTypes {
		res.EstablishmentTypes = append(res.EstablishmentTypes, string(v))
	}

	for _, v := range args.DishCategories {
		res.DishCategories = append(res.DishCategories, string(v))
	}

	for _, v := range args.CuisineTypes {
		res.CuisineTypes = append(res.CuisineTypes, string(v))
	}

	for _, v := range args.SuitableCases {
		res.SuitableCases = append(res.SuitableCases, string(v))
	}

	for _, v := range args.AdditionalServices {
		res.AdditionalServices = append(res.AdditionalServices, string(v))
	}

	return res
}
