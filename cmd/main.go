package main

import (
	"establishment-service/internal/config"
	"establishment-service/internal/errproc"
	"establishment-service/internal/repo"
	"establishment-service/internal/repo/repoFunctions"
	"establishment-service/internal/server"
	"establishment-service/internal/server/handlers"
	"establishment-service/internal/server/middleware"
	"github.com/Restream/reindexer"
	dadata "github.com/ekomobile/dadata/v2"
	"github.com/ekomobile/dadata/v2/api/suggest"
	"github.com/ekomobile/dadata/v2/client"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const Version = "v1.0.0"

func main() {

	logger := logrus.New()
	if logger == nil {
		logger.Fatal("New logger failed")
	}

	// Init configuration and credentials
	configuration, err := config.NewConfiguration()
	if err != nil {
		logger.Fatal("config.NewConfiguration:", err.Error())
	}

	credentials, err := config.NewCredentials()
	if err != nil {
		logger.Fatal("config.NewCredentials:", err.Error())
	}

	// Соединение с БД Reindexer
	DBReindexerConn, err := repo.NewReindexerConnect(credentials.Reindexer)
	if err != nil {
		logger.WithError(err).Fatal("newReindexerConnect - error")
	}

	daDataApi := dadata.NewSuggestApi(client.WithCredentialProvider(&client.Credentials{
		ApiKeyValue:    credentials.DaData.ApiKey,
		SecretKeyValue: credentials.DaData.SecretKey,
	}))

	repositoryRegistry := InitRepoRegistry(DBReindexerConn)
	errproc := errproc.NewErrProc()

	route := InitHandlers(InitHandlersDep{
		RepositoryRegistry: repositoryRegistry,
		Conf:               configuration,
		Credentials:        credentials,
		Logger:             logger,
		ErrProc:            errproc,
		DaDataApi:          daDataApi,
	})

	srv := server.NewServer(server.Options{
		ReadTimeout:       configuration.WebServer.Configuration.ReadTimeout,
		WriteTimeout:      configuration.WebServer.Configuration.WriteTimeout,
		ShutdownTimeout:   configuration.WebServer.Configuration.ShutdownTimeout,
		ReadHeaderTimeout: configuration.WebServer.Configuration.ReadHeaderTimeout,
		IdleTimeout:       configuration.WebServer.Configuration.IdleTimeout,
		MaxHeaderBytes:    configuration.WebServer.Configuration.MaxHeaderBytes,
	}, server.Dependencies{
		Handler: route,
		Logger:  logger,
		Config:  configuration,
	})
	go func() {
		err := srv.Start()
		if err != nil {
			logger.Fatalf("failed to Start server; err - %v", err)
		}
	}()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-done
	logger.Error("Server Stopped")

	err = srv.Stop()
	if err != nil {
		logger.Error("failed to correct Stop server; err - %v", err)
	}
	time.Sleep(time.Second * 3)
	logger.Error("Server Exited Properly")
}

func InitRepoRegistry(DBConnect *reindexer.Reindexer) *repo.Registry {
	return &repo.Registry{
		DishesRepo:         repoFunctions.NewDishesRepo(DBConnect),
		EstablishmentsRepo: repoFunctions.NewEstablishmentsRepo(DBConnect),
		PromotionsRepo:     repoFunctions.NewPromotionsRepo(DBConnect),
		EventsRepo:         repoFunctions.NewEventsRepo(DBConnect),
		ReviewsRepo:        repoFunctions.NewReviewsRepo(DBConnect),
		CollectionsRepo:    repoFunctions.NewCollectionsRepo(DBConnect),
	}
}

func InitHandlers(dep InitHandlersDep) http.Handler {

	requestIDMiddleware := middleware.NewRequestIDMiddleware()
	middlewares := handlers.Middlewares{
		RequestIDMiddleware: requestIDMiddleware,
	}

	establishment, err := handlers.NewEstablishmentHandlers(handlers.EstablishmentDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
		ErrProc:            dep.ErrProc,
		DaDataApi:          dep.DaDataApi,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init NewEstablishmentHandlers")
	}

	dishes, err := handlers.NewDishesHandlers(handlers.DishesDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
		ErrProc:            dep.ErrProc,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init NewDishesHandlers")
	}

	promotions, err := handlers.NewPromotionsHandlers(handlers.PromotionsDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
		ErrProc:            dep.ErrProc,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init NewPromotionsHandlers")
	}

	events, err := handlers.NewEventsHandlers(handlers.EventsDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
		ErrProc:            dep.ErrProc,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init NewEventsHandlers")
	}

	reviews, err := handlers.NewReviewsHandlers(handlers.ReviewsDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
		ErrProc:            dep.ErrProc,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init NewReviewsHandlers")
	}

	collections, err := handlers.NewCollectionsHandlers(handlers.CollectionsDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Logger:             dep.Logger,
		Conf:               dep.Conf,
		ErrProc:            dep.ErrProc,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init NewCollectionsHandlers")
	}

	service, err := handlers.NewServiceHandlers(handlers.ServiceDependencies{
		Logger: dep.Logger,
		Conf:   dep.Conf,
	})

	handlersApi := handlers.Handlers{
		Establishment: establishment,
		Dishes:        dishes,
		Promotions:    promotions,
		Events:        events,
		Reviews:       reviews,
		Collections:   collections,
		Service:       service,
	}

	route := handlers.NewRoute(handlers.RouteDependencies{
		Logger:      dep.Logger,
		Middlewares: middlewares,
		Handlers:    handlersApi,
		Conf:        dep.Conf,
	})

	return route
}

type InitHandlersDep struct {
	RepositoryRegistry *repo.Registry
	Conf               *config.Config
	Credentials        *config.Credentials
	Logger             *logrus.Logger
	ErrProc            *errproc.ErrProc
	DaDataApi          *suggest.Api
}
