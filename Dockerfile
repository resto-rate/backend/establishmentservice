FROM golang:1.21.6 AS builder
WORKDIR /build

ENV CGO_ENABLED=0
ENV GOOS=linux

COPY . .
RUN go mod download
RUN go build -o /main /build/cmd/main.go


FROM alpine:latest AS runner
WORKDIR /app

COPY --from=builder /main /main
COPY conf.yaml .

ENTRYPOINT ["/main"]
